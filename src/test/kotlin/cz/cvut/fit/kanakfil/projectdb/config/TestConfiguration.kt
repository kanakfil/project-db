package cz.cvut.fit.kanakfil.projectdb.config;

import io.kotest.core.config.AbstractProjectConfig
import io.kotest.extensions.spring.SpringExtension

object TestConfiguration : AbstractProjectConfig() {
    override fun extensions() = listOf(SpringExtension)
}
