package cz.cvut.fit.kanakfil.projectdb

import cz.cvut.fit.kanakfil.projectdb.commons.domain.Action
import cz.cvut.fit.kanakfil.projectdb.commons.domain.LifecycleModel
import cz.cvut.fit.kanakfil.projectdb.commons.domain.ProjectStatus
import cz.cvut.fit.kanakfil.projectdb.commons.domain.ProjectType
import cz.cvut.fit.kanakfil.projectdb.commons.event.*
import cz.cvut.fit.kanakfil.projectdb.domain.*
import cz.cvut.fit.kanakfil.projectdb.model.LifecycleModelDTO
import cz.cvut.fit.kanakfil.projectdb.model.ProjectDTO
import cz.cvut.fit.kanakfil.projectdb.model.ProjectStatusHistoryDTO
import cz.cvut.fit.kanakfil.projectdb.repository.*
import cz.cvut.fit.kanakfil.projectdb.service.ProjectService
import io.kotest.core.spec.style.StringSpec
import io.kotest.data.forAll
import io.kotest.data.row
import io.kotest.matchers.shouldBe
import io.mockk.*
import org.springframework.context.ApplicationEventPublisher
import org.springframework.data.repository.findByIdOrNull
import java.time.ZoneId
import java.time.ZonedDateTime
import java.util.*

class ProjectServiceTest : StringSpec({

    DefaultProjectType.UNKNOWN.instance = ProjectType(DefaultProjectType.UNKNOWN.name)
    DefaultProjectType.INTERNAL.instance = ProjectType(DefaultProjectType.INTERNAL.name)
    DefaultProjectStatus.CREATED_AUTOMATICALLY.instance = ProjectStatus(DefaultProjectStatus.CREATED_AUTOMATICALLY.name, listOf())
    DefaultProjectStatus.CONCEPT.instance = ProjectStatus(DefaultProjectStatus.CONCEPT.name, listOf())
    DefaultLifecycle.ISO_IEC_IEEE_24748_1.instance = LifecycleModel(DefaultLifecycle.ISO_IEC_IEEE_24748_1.name, listOf(DefaultProjectStatus.CREATED_AUTOMATICALLY.instance!!))
    DefaultLifecycle.PMBOK.instance = LifecycleModel(DefaultLifecycle.PMBOK.name, listOf(DefaultProjectStatus.CREATED_AUTOMATICALLY.instance!!))

    val projectRepository: ProjectRepository = mockk()
    val applicationEventPublisher: ApplicationEventPublisher = mockk(relaxed = true)
    val projectTypeRepository: ProjectTypeRepository = mockk()
    val projectStatusRepository: ProjectStatusRepository = mockk()
    val projectStatusHistoryRepository: ProjectStatusHistoryRepository = mockk()
    val lifecycleRepository: LifecycleRepository = mockk()
    val actionRepository: ActionRepository = mockk()

    val service = ProjectService(projectRepository, applicationEventPublisher, projectTypeRepository, projectStatusRepository, projectStatusHistoryRepository, lifecycleRepository, actionRepository)

    val projectCapturingSlot = slot<Project>()
    val projectHistoryCapturingSlot = slot<ProjectStatusHistory>()
    every { projectRepository.save(capture(projectCapturingSlot)) } answers {
        projectCapturingSlot.captured.apply { id = 1 }
    }

    every { lifecycleRepository.findByName(any()) } returns DefaultLifecycle.PMBOK.instance
    every { projectTypeRepository.findByName(any()) } returns DefaultProjectType.INTERNAL.instance
    every { projectStatusRepository.findByName(eq("CONCEPT")) } returns DefaultProjectStatus.CONCEPT.instance
    every { projectStatusRepository.findByName(eq("CREATED_AUTOMATICALLY")) } returns DefaultProjectStatus.CREATED_AUTOMATICALLY.instance

    val project = Project("test", model = DefaultLifecycle.ISO_IEC_IEEE_24748_1.instance)
    val projectDTO = ProjectDTO(
        1,
        "test",
        null,
        listOf(ProjectStatusHistoryDTO(DefaultProjectStatus.CREATED_AUTOMATICALLY.name, null, "2000-01-02")),
        LifecycleModelDTO(DefaultLifecycle.ISO_IEC_IEEE_24748_1.name, listOf(DefaultProjectStatus.CREATED_AUTOMATICALLY.name))
    )

    every { projectRepository.findAll() } returns listOf(project.apply { id = 1 })
    every { projectRepository.findByIdOrNull(eq(1)) } returns project.apply { id = 1 }

    every { actionRepository.findByFromAndTo(any(), any()) } returns Action(DefaultProjectStatus.CREATED_AUTOMATICALLY.instance!!, DefaultProjectStatus.CONCEPT.instance!!, listOf())

    "Create project event" {
        every { projectStatusHistoryRepository.save(capture(projectHistoryCapturingSlot)) } answers {
            projectHistoryCapturingSlot.captured.apply { id = 1 }
            every { projectStatusHistoryRepository.findAllByProjectOrderByCreated(neq(project)) } answers { listOf(projectHistoryCapturingSlot.captured) }
            projectHistoryCapturingSlot.captured
        }

        val id = UUID.randomUUID()
        service.listenCreateProject(CreateProjectEvent(id, "TestProject"))

        verify { applicationEventPublisher.publishEvent(eq(ProjectCreatedEvent(id, projectCapturingSlot.captured.id!!))) }
        confirmVerified(applicationEventPublisher)
        projectCapturingSlot.clear()
        projectHistoryCapturingSlot.clear()
    }

    val history =
        listOf(ProjectStatusHistory(project, DefaultProjectStatus.CREATED_AUTOMATICALLY.instance!!, null).apply {
            created = ZonedDateTime.of(2000, 1, 2, 3, 4, 5, 6, ZoneId.of("UTC"))
        })
    every { projectStatusHistoryRepository.findAllByProjectOrderByCreated(eq(project)) } returns history



    "Get projects" {
        service.getProjects() shouldBe listOf(projectDTO)
    }

    "Get project" {
        service.getProject(1) shouldBe projectDTO
    }

    "Update project" {
        forAll(
            row(UpdateProjectModelEvent("PMBOK"), projectDTO.copy(model = LifecycleModelDTO("PMBOK", listOf("CREATED_AUTOMATICALLY")))),
            row(UpdateProjectTypeEvent("INTERNAL"), projectDTO.copy(type = DefaultProjectType.INTERNAL.instance)),
            row(
                UpdateProjectStatusEvent("CREATED_AUTOMATICALLY", "CONCEPT"), projectDTO.copy(
                    statusHistory = listOf(
                        ProjectStatusHistoryDTO(DefaultProjectStatus.CREATED_AUTOMATICALLY.name, null, "2000-01-02"),
                        ProjectStatusHistoryDTO(DefaultProjectStatus.CONCEPT.name, DefaultProjectStatus.CREATED_AUTOMATICALLY.name, "2000-01-02"),
                    )
                )
            ),
        ) { event, result ->
            val p = Project(project.name, project.type, project.model).apply { id = 1 }
            val s = ProjectStatusHistory(p, DefaultProjectStatus.CREATED_AUTOMATICALLY.instance!!, null).apply {
                created = ZonedDateTime.of(2000, 1, 2, 3, 4, 5, 6, ZoneId.of("UTC"))
            }
            every { projectRepository.findByIdOrNull(eq(1)) } returns p
            every { projectStatusHistoryRepository.save(capture(projectHistoryCapturingSlot)) } answers { projectHistoryCapturingSlot.captured }
            every { projectStatusHistoryRepository.findAllByProjectOrderByCreated(eq(p)) } answers {
                if (projectHistoryCapturingSlot.isCaptured) listOf(s, projectHistoryCapturingSlot.captured.apply {
                    created = ZonedDateTime.of(2000, 1, 2, 3, 4, 5, 6, ZoneId.of("UTC"))
                }) else listOf(s)
            }

            service.updateProject(1, event) shouldBe result
            projectCapturingSlot.clear()
        }
    }

    "Lifecycle models" {
        every { lifecycleRepository.findAll() } returns listOf(DefaultLifecycle.ISO_IEC_IEEE_24748_1.instance, DefaultLifecycle.PMBOK.instance)

        service.getLifecycleModels() shouldBe listOf(LifecycleModelDTO("ISO_IEC_IEEE_24748_1", listOf("CREATED_AUTOMATICALLY")), LifecycleModelDTO("PMBOK", listOf("CREATED_AUTOMATICALLY")))

    }

    "Project types" {
        every { projectTypeRepository.findAll() } returns listOf(DefaultProjectType.INTERNAL.instance, DefaultProjectType.UNKNOWN.instance)

        service.getProjectTypes() shouldBe listOf("INTERNAL", "UNKNOWN")

    }


})
