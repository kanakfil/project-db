package cz.cvut.fit.kanakfil.projectdb

import com.fasterxml.jackson.databind.ObjectMapper
import com.ninjasquad.springmockk.MockkBean
import cz.cvut.fit.kanakfil.projectdb.commons.domain.ProjectStatus
import cz.cvut.fit.kanakfil.projectdb.commons.domain.ProjectType
import cz.cvut.fit.kanakfil.projectdb.commons.event.CreateProjectEvent
import cz.cvut.fit.kanakfil.projectdb.commons.event.UpdateProjectTypeEvent
import cz.cvut.fit.kanakfil.projectdb.controllers.ProjectController
import cz.cvut.fit.kanakfil.projectdb.domain.DefaultProjectStatus
import cz.cvut.fit.kanakfil.projectdb.model.LifecycleModelDTO
import cz.cvut.fit.kanakfil.projectdb.model.ProjectDTO
import cz.cvut.fit.kanakfil.projectdb.service.ProjectService
import io.kotest.core.spec.style.StringSpec
import io.mockk.every
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.http.MediaType
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import java.util.*


@WebMvcTest(ProjectController::class)
@ContextConfiguration
@AutoConfigureMockMvc(addFilters = false)
@ActiveProfiles("test")
class ProjectControllerTest(@Autowired val mockMvc: MockMvc) : StringSpec() {

    @MockkBean
    lateinit var projectService: ProjectService

    @Autowired
    lateinit var objectMapper: ObjectMapper


    init {

        DefaultProjectStatus.CREATED_MANUALLY.instance = ProjectStatus(DefaultProjectStatus.CREATED_MANUALLY.name, listOf())

        "Project list" {
            every { projectService.getProjects() } returns listOf(ProjectDTO(1, "test", ProjectType("INTERNAL"), listOf(), LifecycleModelDTO("model", listOf())))

            mockMvc.perform(get("/api/projects")).andExpect(status().isOk).andExpect(content().contentType(MediaType.APPLICATION_JSON)).andExpect(jsonPath("$.*.name").value("test"))
        }

        "Create project" {
            every { projectService.createProject(any()) } returns ProjectDTO(1, "test", ProjectType("INTERNAL"), listOf(), LifecycleModelDTO("model", listOf()))

            mockMvc.perform(
                post("/api/projects")
                    .contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(CreateProjectEvent(UUID.randomUUID(), "Project")))
            ).andExpect(status().isOk).andExpect(jsonPath("$.name").value("test"))
        }

        "Create project missing body" {
            every { projectService.createProject(any()) } returns ProjectDTO(1, "test", ProjectType("INTERNAL"), listOf(), LifecycleModelDTO("model", listOf()))

            mockMvc.perform(post("/api/projects")).andExpect(status().isBadRequest)
        }

        "Update project" {
            every { projectService.updateProject(any(), any()) } returns ProjectDTO(1, "test", ProjectType("INTERNAL"), listOf(), LifecycleModelDTO("model", listOf()))

            mockMvc.perform(
                put("/api/projects/1")
                    .contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(UpdateProjectTypeEvent("INTERNAL")))
            ).andExpect(status().isOk).andExpect(jsonPath("$.name").value("test"))
        }

        "Get project" {
            every { projectService.getProject(eq(1)) } returns ProjectDTO(1, "test", ProjectType("INTERNAL"), listOf(), LifecycleModelDTO("model", listOf()))

            mockMvc.perform(get("/api/projects/1")).andExpect(status().isOk).andExpect(jsonPath("$.name").value("test"))
        }

        "Get types" {
            every { projectService.getProjectTypes() } returns listOf("TYPE1", "TYPE2")

            mockMvc.perform(get("/api/projects/types")).andExpect(status().isOk).andExpect(content().contentType(MediaType.APPLICATION_JSON)).andExpect(jsonPath("$.[0]").value("TYPE1"))
        }

        "Get models" {
            every { projectService.getLifecycleModels() } returns listOf(LifecycleModelDTO("model", listOf()))

            mockMvc.perform(get("/api/projects/lifecycleModels")).andExpect(status().isOk).andExpect(content().contentType(MediaType.APPLICATION_JSON)).andExpect(jsonPath("$.[0].name").value("model"))
        }

    }
}
