package cz.cvut.fit.kanakfil.projectdb

import cz.cvut.fit.kanakfil.projectdb.commons.domain.Action
import cz.cvut.fit.kanakfil.projectdb.commons.domain.ModuleAction
import cz.cvut.fit.kanakfil.projectdb.commons.domain.ProjectStatus
import cz.cvut.fit.kanakfil.projectdb.commons.repository.ModuleActionRepository
import cz.cvut.fit.kanakfil.projectdb.model.ActionDTO
import cz.cvut.fit.kanakfil.projectdb.repository.ActionRepository
import cz.cvut.fit.kanakfil.projectdb.service.ActionService
import cz.cvut.fit.kanakfil.projectdb.service.ProjectService
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import io.mockk.*
import org.springframework.web.server.ResponseStatusException

class ActionServiceTest : StringSpec({
    val moduleActionRepository: ModuleActionRepository = mockk()
    val actionRepository: ActionRepository = mockk()
    val projectService: ProjectService = mockk()

    val service = ActionService(moduleActionRepository, actionRepository, projectService)

    "All actions" {
        every { actionRepository.findAll() } returns listOf(Action(ProjectStatus("SOME", listOf()), ProjectStatus("OTHER", listOf()), listOf()))

        service.getActions() shouldBe listOf(ActionDTO("SOME", "OTHER", listOf()))
    }

    "Available module actions" {
        val list = listOf(ModuleAction("TEST_MODULE", "TEST_APPLICATION"))
        every { moduleActionRepository.findAll() } returns list

        service.getAvailable() shouldBe list
        verify { moduleActionRepository.findAll() }
    }

    "Create action" {
        every { projectService.getProjectStatus(eq("SOME")) } returns ProjectStatus("SOME", listOf())
        every { projectService.getProjectStatus(eq("OTHER")) } returns ProjectStatus("OTHER", listOf())
        every { actionRepository.existsByFromAndTo(any(), any()) } returns false
        every { actionRepository.save(any()) } returnsArgument 0
        every { moduleActionRepository.findByModuleAndAction(any(), any()) } returns ModuleAction("", "")

        service.createAction(
            ActionDTO(
                "SOME", "OTHER", listOf(
                    ModuleAction("TEST_MODULE", "TEST_ACTION"),
                    ModuleAction("TSECOND_MODULE", "SECOND_ACTION")
                )
            )
        )

        verify(exactly = 2) { moduleActionRepository.findByModuleAndAction(any(), any()) }

        confirmVerified(moduleActionRepository)
    }

    "Create action conflict" {
        every { projectService.getProjectStatus(any()) } returns ProjectStatus("OTHER", listOf())
        every { actionRepository.existsByFromAndTo(any(), any()) } returns true

        shouldThrow<ResponseStatusException> {
            service.createAction(ActionDTO("SOME", "OTHER", listOf()))
        }
    }

    "Delete action" {
        val some = ProjectStatus("SOME", listOf())
        val other = ProjectStatus("OTHER", listOf())
        every { projectService.getProjectStatus(eq("SOME")) } returns some
        every { projectService.getProjectStatus(eq("OTHER")) } returns other
        every { actionRepository.findByFromAndTo(eq(some), eq(other)) } returns Action(some, other, listOf())
        every { actionRepository.delete(any()) } just Runs

        service.deleteAction("SOME", "OTHER")

        verify { actionRepository.delete(any()) }
    }
})
