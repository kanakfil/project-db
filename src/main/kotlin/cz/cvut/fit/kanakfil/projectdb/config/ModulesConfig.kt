package cz.cvut.fit.kanakfil.projectdb.config

import cz.cvut.fit.kanakfil.projectdb.identitymanagement.keycloak.config.WebSecurityConfig
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import org.springframework.context.annotation.Import

@ConstructorBinding
@ConfigurationProperties(prefix = "modules")
@Import(WebSecurityConfig::class)
class ModulesConfig
