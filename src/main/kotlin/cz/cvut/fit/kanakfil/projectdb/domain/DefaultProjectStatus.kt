package cz.cvut.fit.kanakfil.projectdb.domain

import cz.cvut.fit.kanakfil.projectdb.commons.domain.ProjectStatus

enum class DefaultProjectStatus : DefaultInstance<ProjectStatus> {
    PREPARATION,
    OFFER_IN_PROCESS,
    PROPOSED,
    FEASIBILITY,
    CREATED_AUTOMATICALLY,
    CREATED_MANUALLY,
    CONCEPT,
    DEVELOPMENT,
    PRODUCTION,
    UTILIZATION,
    SUPPORT,
    CLOSED,
    TESTING;

    override var instance: ProjectStatus? = null
}
