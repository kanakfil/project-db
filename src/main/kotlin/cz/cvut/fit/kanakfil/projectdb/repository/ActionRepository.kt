package cz.cvut.fit.kanakfil.projectdb.repository;

import cz.cvut.fit.kanakfil.projectdb.commons.domain.Action
import cz.cvut.fit.kanakfil.projectdb.commons.domain.ProjectStatus
import org.springframework.data.repository.CrudRepository

interface ActionRepository : CrudRepository<Action, Long> {
    fun findByFromAndTo(from: ProjectStatus, to: ProjectStatus): Action?
    fun existsByFromAndTo(from: ProjectStatus, to: ProjectStatus): Boolean
}
