package cz.cvut.fit.kanakfil.projectdb.domain

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id

@Entity
class RoleType(
    @Column
    val type: String
) {
    @Id
    @Column(name = "id", nullable = false)
    var id: Long? = null
}
