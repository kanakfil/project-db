package cz.cvut.fit.kanakfil.projectdb.domain

import cz.cvut.fit.kanakfil.projectdb.commons.domain.ProjectType

enum class DefaultProjectType : DefaultInstance<ProjectType> {
    INTERNAL,
    EXTERNAL,
    PRODUCT,
    INVESTMENT,
    IT,
    ORGANIZATIONAL,
    UNKNOWN;

    override var instance: ProjectType? = null
}
