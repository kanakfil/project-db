package cz.cvut.fit.kanakfil.projectdb.domain

import cz.cvut.fit.kanakfil.projectdb.commons.domain.LifecycleModel

enum class DefaultLifecycle : DefaultInstance<LifecycleModel> {
    PMBOK,
    ISO_IEC_IEEE_24748_1;

    override var instance: LifecycleModel? = null
}
