package cz.cvut.fit.kanakfil.projectdb.model

import cz.cvut.fit.kanakfil.projectdb.commons.domain.ModuleAction

data class ActionDTO(val from: String, val to: String, val moduleActions: List<ModuleAction>)
