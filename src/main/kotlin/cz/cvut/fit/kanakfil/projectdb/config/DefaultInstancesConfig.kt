package cz.cvut.fit.kanakfil.projectdb.config

import cz.cvut.fit.kanakfil.projectdb.domain.DefaultLifecycle
import cz.cvut.fit.kanakfil.projectdb.domain.DefaultProjectStatus
import cz.cvut.fit.kanakfil.projectdb.domain.DefaultProjectType
import cz.cvut.fit.kanakfil.projectdb.repository.LifecycleRepository
import cz.cvut.fit.kanakfil.projectdb.repository.ProjectStatusRepository
import cz.cvut.fit.kanakfil.projectdb.repository.ProjectTypeRepository
import cz.cvut.fit.kanakfil.projectdb.util.logger
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component

@Component
class DefaultInstancesConfig(
    val projectStatusRepository: ProjectStatusRepository,
    val projectTypeRepository: ProjectTypeRepository,
    val lifecycleRepository: LifecycleRepository
) {

    @EventListener(ApplicationReadyEvent::class)
    fun loadInstances() {
        DefaultProjectStatus.values().forEach { it.instance = projectStatusRepository.findByName(it.name) }
        DefaultProjectType.values().forEach { it.instance = projectTypeRepository.findByName(it.name) }
        DefaultLifecycle.values().forEach { it.instance = lifecycleRepository.findByName(it.name) }
        logger().info("Default instances from DB loaded")
    }
}
