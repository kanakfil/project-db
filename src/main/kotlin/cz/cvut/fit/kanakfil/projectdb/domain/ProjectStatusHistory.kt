package cz.cvut.fit.kanakfil.projectdb.domain

import cz.cvut.fit.kanakfil.projectdb.commons.domain.ProjectStatus
import org.hibernate.annotations.CreationTimestamp
import java.time.ZonedDateTime
import javax.persistence.*

@Entity
class ProjectStatusHistory(
    @ManyToOne
    val project: Project,
    @ManyToOne
    val status: ProjectStatus,
    @ManyToOne
    val previous: ProjectStatus?
) {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    var id: Long? = null

    @CreationTimestamp
    var created: ZonedDateTime? = null

}
