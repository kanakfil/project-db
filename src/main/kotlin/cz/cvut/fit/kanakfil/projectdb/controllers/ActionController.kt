package cz.cvut.fit.kanakfil.projectdb.controllers

import cz.cvut.fit.kanakfil.projectdb.commons.domain.ModuleAction
import cz.cvut.fit.kanakfil.projectdb.model.ActionDTO
import cz.cvut.fit.kanakfil.projectdb.service.ActionService
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/actions")
class ActionController(val actionService: ActionService) {

    @GetMapping
    @PreAuthorize("hasRole('USER')")
    fun getActions(): List<ActionDTO> {
        return actionService.getActions()
    }

    @GetMapping("/available")
    @PreAuthorize("hasRole('ADMIN')")
    fun getAvailableActions(): List<ModuleAction> {
        return actionService.getAvailable()
    }

    @PostMapping
    @PreAuthorize("hasRole('ADMIN')")
    fun createAction(@RequestBody action: ActionDTO): List<ActionDTO> {
        actionService.createAction(action)
        return actionService.getActions()
    }

    @DeleteMapping
    @PreAuthorize("hasRole('ADMIN')")
    fun deleteAction(@RequestParam from: String, @RequestParam to: String): List<ActionDTO> {
        actionService.deleteAction(from, to)
        return actionService.getActions()
    }

}
