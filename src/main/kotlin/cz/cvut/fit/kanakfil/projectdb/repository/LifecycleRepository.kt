package cz.cvut.fit.kanakfil.projectdb.repository;

import cz.cvut.fit.kanakfil.projectdb.commons.domain.LifecycleModel
import org.springframework.data.repository.CrudRepository

interface LifecycleRepository : CrudRepository<LifecycleModel, Long> {
    fun findByName(name: String): LifecycleModel?
}
