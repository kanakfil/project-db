package cz.cvut.fit.kanakfil.projectdb.domain

import cz.cvut.fit.kanakfil.projectdb.commons.domain.LifecycleModel
import cz.cvut.fit.kanakfil.projectdb.commons.domain.ProjectStatus
import cz.cvut.fit.kanakfil.projectdb.commons.domain.ProjectType
import javax.persistence.*

@Entity
class Project(
    @Column(name = "name", nullable = false)
    var name: String? = null,

    @ManyToOne
    var type: ProjectType? = null,

    @ManyToOne
    var model: LifecycleModel? = null
) {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    var id: Long? = null
}
