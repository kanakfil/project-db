package cz.cvut.fit.kanakfil.projectdb.repository

import cz.cvut.fit.kanakfil.projectdb.commons.domain.ProjectType
import org.springframework.data.repository.CrudRepository
import java.util.*

interface ProjectTypeRepository : CrudRepository<ProjectType, UUID> {
    fun findByName(name: String): ProjectType?
}
