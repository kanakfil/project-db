package cz.cvut.fit.kanakfil.projectdb.repository;

import cz.cvut.fit.kanakfil.projectdb.commons.domain.ProjectStatus
import org.springframework.data.repository.CrudRepository

interface ProjectStatusRepository : CrudRepository<ProjectStatus, Long> {
    fun findByName(name: String): ProjectStatus?
}
