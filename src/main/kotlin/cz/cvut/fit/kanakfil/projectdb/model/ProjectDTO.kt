package cz.cvut.fit.kanakfil.projectdb.model

import cz.cvut.fit.kanakfil.projectdb.commons.domain.LifecycleModel
import cz.cvut.fit.kanakfil.projectdb.commons.domain.ProjectType
import cz.cvut.fit.kanakfil.projectdb.domain.Project
import cz.cvut.fit.kanakfil.projectdb.domain.ProjectStatusHistory
import java.time.format.DateTimeFormatter

data class ProjectDTO(val id: Long?, val name: String?, val type: ProjectType?, val statusHistory: List<ProjectStatusHistoryDTO>, val model: LifecycleModelDTO?) {
    constructor(project: Project, statusHistory: List<ProjectStatusHistory>) : this(project.id, project.name, project.type, statusHistory.map { ProjectStatusHistoryDTO(it) }, project.model?.let { LifecycleModelDTO(it) })
}

data class LifecycleModelDTO(val name: String, val projectStatuses: List<String>) {
    constructor(lifecycleModel: LifecycleModel) : this(lifecycleModel.name, lifecycleModel.projectStatuses.map { it.name })
}

data class ProjectStatusHistoryDTO(val projectStatus: String, val previous: String?, val date: String?) {
    constructor(projectStatusHistory: ProjectStatusHistory) : this(projectStatusHistory.status.name, projectStatusHistory.previous?.name, projectStatusHistory.created?.format(DateTimeFormatter.ISO_LOCAL_DATE))
}
