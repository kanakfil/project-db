package cz.cvut.fit.kanakfil.projectdb.service

import cz.cvut.fit.kanakfil.projectdb.commons.domain.ProjectStatus
import cz.cvut.fit.kanakfil.projectdb.commons.event.*
import cz.cvut.fit.kanakfil.projectdb.domain.*
import cz.cvut.fit.kanakfil.projectdb.model.LifecycleModelDTO
import cz.cvut.fit.kanakfil.projectdb.model.ProjectDTO
import cz.cvut.fit.kanakfil.projectdb.repository.*
import cz.cvut.fit.kanakfil.projectdb.util.logger
import org.springframework.context.ApplicationEventPublisher
import org.springframework.context.event.EventListener
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException

@Service
class ProjectService(
    private val projectRepository: ProjectRepository,
    private val applicationEventPublisher: ApplicationEventPublisher,
    private val projectTypeRepository: ProjectTypeRepository,
    private val projectStatusRepository: ProjectStatusRepository,
    private val projectStatusHistoryRepository: ProjectStatusHistoryRepository,
    private val lifecycleRepository: LifecycleRepository,
    private val actionRepository: ActionRepository
) : UpdateEventVisitor {

    @EventListener
    fun listenCreateProject(event: CreateProjectEvent) {
        logger().debug("Received CreateProjectEvent: $", event)
        val created = createProject(event)
        if (created.id != null) {
            applicationEventPublisher.publishEvent(ProjectCreatedEvent(event.id, created.id))
        }
    }

    fun createProject(event: CreateProjectEvent): ProjectDTO {
        var project = Project(
            event.name,
            DefaultProjectType.UNKNOWN.instance,
            DefaultLifecycle.ISO_IEC_IEEE_24748_1.instance
        )
        val psh = ProjectStatusHistory(
            project,
            (event.status ?: DefaultProjectStatus.CREATED_AUTOMATICALLY.instance)!!,
            null
        )

        project = projectRepository.save(project)
        projectStatusHistoryRepository.save(psh)
        val history = projectStatusHistoryRepository.findAllByProjectOrderByCreated(project)
        return ProjectDTO(project, history)
    }

    fun getProjects(): List<ProjectDTO> {
        return projectRepository.findAll().map {
            ProjectDTO(it, projectStatusHistoryRepository.findAllByProjectOrderByCreated(it))
        }
    }

    fun getProject(id: Long): ProjectDTO? {
        return projectRepository.findByIdOrNull(id)?.let {
            ProjectDTO(it, projectStatusHistoryRepository.findAllByProjectOrderByCreated(it))
        }
    }

    fun updateProject(id: Long, updateProjectEvent: UpdateProjectEvent): ProjectDTO? {
        return projectRepository.findByIdOrNull(id)?.let {
            updateProjectEvent.process(this, id)

            ProjectDTO(it, projectStatusHistoryRepository.findAllByProjectOrderByCreated(it))
        } ?: throw ResponseStatusException(HttpStatus.NOT_FOUND)
    }

    override fun visit(updateProjectStatusEvent: UpdateProjectStatusEvent, projectId: Long) {
        projectRepository.findByIdOrNull(projectId)?.let {
            val from = getProjectStatus(updateProjectStatusEvent.from)
            val to = getProjectStatus(updateProjectStatusEvent.to)
            projectStatusHistoryRepository.save(
                ProjectStatusHistory(it, to, from)
            )

            actionRepository.findByFromAndTo(from, to)?.let { action ->
                applicationEventPublisher.publishEvent(ProjectStatusUpdatedEvent(projectId, it.name!!, action))
            }
        }
    }

    override fun visit(updateProjectTypeEvent: UpdateProjectTypeEvent, projectId: Long) {
        projectRepository.findByIdOrNull(projectId)?.let {
            val type = projectTypeRepository.findByName(updateProjectTypeEvent.to) ?: throw ResponseStatusException(HttpStatus.NOT_FOUND)
            it.type = type
            projectRepository.save(it)
        }
    }

    override fun visit(updateProjectModelEvent: UpdateProjectModelEvent, projectId: Long) {
        projectRepository.findByIdOrNull(projectId)?.let {
            val model = lifecycleRepository.findByName(updateProjectModelEvent.to) ?: throw ResponseStatusException(HttpStatus.NOT_FOUND)
            it.model = model
            projectRepository.save(it)
        }
    }

    fun getProjectStatus(status: String): ProjectStatus {
        return projectStatusRepository.findByName(status) ?: throw ResponseStatusException(HttpStatus.NOT_FOUND)
    }

    fun getProjectTypes(): List<String> {
        return projectTypeRepository.findAll().toList().map { it.name }
    }

    fun getLifecycleModels(): List<LifecycleModelDTO> {
        return lifecycleRepository.findAll().map { LifecycleModelDTO(it) }
    }
}
