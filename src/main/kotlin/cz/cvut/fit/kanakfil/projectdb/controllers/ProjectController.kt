package cz.cvut.fit.kanakfil.projectdb.controllers

import cz.cvut.fit.kanakfil.projectdb.commons.event.CreateProjectEvent
import cz.cvut.fit.kanakfil.projectdb.commons.event.UpdateProjectEvent
import cz.cvut.fit.kanakfil.projectdb.domain.DefaultProjectStatus
import cz.cvut.fit.kanakfil.projectdb.model.LifecycleModelDTO
import cz.cvut.fit.kanakfil.projectdb.model.ProjectDTO
import cz.cvut.fit.kanakfil.projectdb.service.ProjectService
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*


@RestController
@RequestMapping("/api/projects")
class ProjectController(val projectService: ProjectService) {

    @GetMapping
    @PreAuthorize("hasRole('USER')")
    fun getProjects(): ResponseEntity<List<ProjectDTO>> =
        ResponseEntity.ok(projectService.getProjects())

    @PostMapping
    @PreAuthorize("hasRole('ADMIN')")
    fun createProject(@RequestBody createProjectEvent: CreateProjectEvent): ResponseEntity<ProjectDTO> = ResponseEntity.ok(projectService.createProject(createProjectEvent.copy(status = DefaultProjectStatus.CREATED_MANUALLY.instance)))

    @PutMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    fun updateProject(@PathVariable id: Long, @RequestBody updateProjectEvent: UpdateProjectEvent): ResponseEntity<ProjectDTO> = ResponseEntity.ok(projectService.updateProject(id, updateProjectEvent))

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('USER')")
    fun getProject(@PathVariable id: Long): ResponseEntity<ProjectDTO> = ResponseEntity.ok(projectService.getProject(id))

    @GetMapping("/types")
    @PreAuthorize("hasRole('USER')")
    fun getTypes(): ResponseEntity<List<String>> = ResponseEntity.ok(projectService.getProjectTypes())

    @GetMapping("/lifecycleModels")
    @PreAuthorize("hasRole('USER')")
    fun getLifecycleModels(): ResponseEntity<List<LifecycleModelDTO>> = ResponseEntity.ok(projectService.getLifecycleModels())

}
