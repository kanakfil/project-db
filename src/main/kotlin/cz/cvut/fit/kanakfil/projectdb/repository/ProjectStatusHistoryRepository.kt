package cz.cvut.fit.kanakfil.projectdb.repository

import cz.cvut.fit.kanakfil.projectdb.domain.Project
import cz.cvut.fit.kanakfil.projectdb.domain.ProjectStatusHistory
import org.springframework.data.repository.CrudRepository

interface ProjectStatusHistoryRepository : CrudRepository<ProjectStatusHistory, Long> {
    fun findAllByProjectOrderByCreated(project: Project): List<ProjectStatusHistory>
}
