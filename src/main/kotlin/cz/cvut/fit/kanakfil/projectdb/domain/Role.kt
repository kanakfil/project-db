package cz.cvut.fit.kanakfil.projectdb.domain

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.ManyToOne

@Entity
class Role(

    @ManyToOne
    val roleType: RoleType? = null,

    @ManyToOne
    val stakeholder: Stakeholder? = null,

    @ManyToOne
    val project: Project? = null
) {
    @Id
    @Column(name = "id", nullable = false)
    open var id: Long? = null

}
