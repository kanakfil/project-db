package cz.cvut.fit.kanakfil.projectdb.repository

import cz.cvut.fit.kanakfil.projectdb.domain.Project
import org.springframework.data.repository.CrudRepository

interface ProjectRepository : CrudRepository<Project, Long>
