package cz.cvut.fit.kanakfil.projectdb.domain

interface DefaultInstance<T> {
    var instance: T?
}
