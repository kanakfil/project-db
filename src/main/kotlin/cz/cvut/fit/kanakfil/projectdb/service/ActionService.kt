package cz.cvut.fit.kanakfil.projectdb.service

import cz.cvut.fit.kanakfil.projectdb.commons.domain.Action
import cz.cvut.fit.kanakfil.projectdb.commons.domain.ModuleAction
import cz.cvut.fit.kanakfil.projectdb.commons.repository.ModuleActionRepository
import cz.cvut.fit.kanakfil.projectdb.model.ActionDTO
import cz.cvut.fit.kanakfil.projectdb.repository.ActionRepository
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException

@Service
class ActionService(
    private val moduleActionRepository: ModuleActionRepository,
    private val actionRepository: ActionRepository,
    private val projectService: ProjectService
) {
    fun getAvailable(): List<ModuleAction> {
        return moduleActionRepository.findAll().toList()
    }

    fun getActions(): List<ActionDTO> {
        return actionRepository.findAll().map {
            ActionDTO(it.from.name, it.to.name, it.moduleActions)
        }
    }

    fun createAction(action: ActionDTO) {
        val from = projectService.getProjectStatus(action.from)
        val to = projectService.getProjectStatus(action.to)
        if (actionRepository.existsByFromAndTo(from, to)) {
            throw ResponseStatusException(HttpStatus.CONFLICT)
        }

        actionRepository.save(Action(from, to, action.moduleActions.map {
            moduleActionRepository.findByModuleAndAction(it.module, it.action)
        }))
    }

    fun deleteAction(from: String, to: String) {
        val fromStatus = projectService.getProjectStatus(from)
        val toStatus = projectService.getProjectStatus(to)

        actionRepository.findByFromAndTo(fromStatus, toStatus)?.let {
            actionRepository.delete(it)
        }
    }

}
