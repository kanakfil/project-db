package cz.cvut.fit.kanakfil.projectdb.domain

import org.hibernate.annotations.GenericGenerator
import org.hibernate.annotations.Type
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
class Stakeholder(

    @Column(name = "name", nullable = false)
    val name: String? = null,

    @Column(name = "username", nullable = false)
    val username: String? = null
) {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "id", nullable = false)
    @Type(type = "uuid-char")
    var id: UUID? = null
}
