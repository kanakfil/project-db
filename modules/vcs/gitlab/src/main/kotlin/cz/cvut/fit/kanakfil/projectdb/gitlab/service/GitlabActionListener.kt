package cz.cvut.fit.kanakfil.projectdb.gitlab.service

import cz.cvut.fit.kanakfil.projectdb.commons.domain.ModuleAction
import cz.cvut.fit.kanakfil.projectdb.commons.event.ProjectStatusUpdatedEvent
import cz.cvut.fit.kanakfil.projectdb.commons.repository.ModuleActionRepository
import cz.cvut.fit.kanakfil.projectdb.gitlab.GitlabModule.Companion.MODULE_NAME
import cz.cvut.fit.kanakfil.projectdb.gitlab.client.GitlabProperties
import cz.cvut.fit.kanakfil.projectdb.gitlab.model.CreateProject
import org.springframework.context.event.EventListener
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Component

@Component
class GitlabActionListener(
    private val gitlabProjectService: GitlabProjectService,
    private val gitlabProperties: GitlabProperties,
    private val moduleActionRepository: ModuleActionRepository,
) {

    @Async
    @EventListener
    fun listenAction(event: ProjectStatusUpdatedEvent) {

        event.action.moduleActions.filter { it.module == MODULE_NAME }.map {
            if (it.action == "CREATE_PROJECT") {
                gitlabProjectService.createProject(CreateProject(event.projectName), event.projectId)
            }
        }
    }

    fun registerActions() {
        gitlabProperties.actions.map {
            if (!moduleActionRepository.existsByModuleAndAction(MODULE_NAME, it)) {
                moduleActionRepository.save(ModuleAction(MODULE_NAME, it))
            }
        }
    }
}
