package cz.cvut.fit.kanakfil.projectdb.gitlab.config

import org.springframework.context.annotation.Configuration
import org.springframework.scheduling.annotation.EnableAsync

@Configuration
@EnableAsync
class ListenerConfiguration {
}
