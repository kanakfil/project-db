package cz.cvut.fit.kanakfil.projectdb.gitlab.client

import cz.cvut.fit.kanakfil.projectdb.gitlab.model.CreateProject
import cz.cvut.fit.kanakfil.projectdb.gitlab.model.ProjectDetail
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam

interface GitlabClient {

    fun getProjectDetail(@PathVariable("projectId") projectId: Long): ProjectDetail
    fun getNewAndModifiedProjects(@RequestParam("last_activity_after") activity_after: String): List<ProjectDetail>
    fun createProject(@RequestBody createGitlabProject: CreateProject): ProjectDetail

}
