package cz.cvut.fit.kanakfil.projectdb.gitlab

import cz.cvut.fit.kanakfil.projectdb.commons.service.JobScheduler
import cz.cvut.fit.kanakfil.projectdb.commons.service.SchedulerParams
import cz.cvut.fit.kanakfil.projectdb.gitlab.job.GitlabSyncJob
import cz.cvut.fit.kanakfil.projectdb.gitlab.service.GitlabActionListener
import cz.cvut.fit.kanakfil.projectdb.util.logger
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component

@Component
@ConditionalOnProperty(name = ["modules.vcs.gitlab"], havingValue = "enabled")
class GitlabModule(private val jobScheduler: JobScheduler, private val gitlabActionListener: GitlabActionListener) {

    @EventListener(ApplicationReadyEvent::class)
    fun loadModule() {
        logger().info("Loaded module: gitlab")
        gitlabActionListener.registerActions()
        jobScheduler.scheduleJob(
            GitlabSyncJob.JOB_NAME,
            SchedulerParams(21, 34),
            GitlabSyncJob::class.java
        )
    }

    companion object {
        const val MODULE_NAME = "GITLAB"
    }
}
