package cz.cvut.fit.kanakfil.projectdb.gitlab.repository

import cz.cvut.fit.kanakfil.projectdb.raynet.domain.GitlabProject
import org.springframework.data.repository.CrudRepository
import java.util.*

interface GitlabProjectRepository : CrudRepository<GitlabProject, UUID> {
    fun getByGitlabId(id: Long): GitlabProject?
    fun findAllByProjectId(id: Long): List<GitlabProject>
}
