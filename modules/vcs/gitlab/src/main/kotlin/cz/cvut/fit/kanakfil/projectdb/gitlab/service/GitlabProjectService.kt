package cz.cvut.fit.kanakfil.projectdb.gitlab.service

import cz.cvut.fit.kanakfil.projectdb.commons.service.SynchronizationService
import cz.cvut.fit.kanakfil.projectdb.gitlab.GitlabModule.Companion.MODULE_NAME
import cz.cvut.fit.kanakfil.projectdb.gitlab.client.Gitlab4jClient
import cz.cvut.fit.kanakfil.projectdb.gitlab.client.GitlabFeignClient
import cz.cvut.fit.kanakfil.projectdb.gitlab.model.CreateProject
import cz.cvut.fit.kanakfil.projectdb.gitlab.model.GitlabProjectDTO
import cz.cvut.fit.kanakfil.projectdb.gitlab.model.ProjectDetail
import cz.cvut.fit.kanakfil.projectdb.gitlab.repository.GitlabProjectRepository
import cz.cvut.fit.kanakfil.projectdb.raynet.domain.GitlabProject
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit
import java.util.*

@Service
class GitlabProjectService(
    private val gitlabFeignClient: GitlabFeignClient,
    private val gitlab4jClient: Gitlab4jClient,
    private val gitlabProjectRepository: GitlabProjectRepository,
    private val synchronizationService: SynchronizationService
) {

    fun updateProject(project: ProjectDetail, projectLink: Long? = null) {
        (gitlabProjectRepository.getByGitlabId(project.id) ?: GitlabProject(project.id))
            .apply {
                name = project.name
                lastActivityAt = project.last_activity_at
                projectLink?.let { projectId = projectLink }
            }
            .let(gitlabProjectRepository::save)
    }

    fun updateAll() {
        val lastSync = synchronizationService.getLastSync(MODULE_NAME)
        gitlabFeignClient.getNewAndModifiedProjects(
            lastSync.lastSync.format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'"))
        ).map(this::updateProject)
        synchronizationService.syncedNow(MODULE_NAME)
    }

    fun getProjects(): List<GitlabProjectDTO> {
        return gitlabProjectRepository.findAll().map(this::getDTO)
    }

    fun getProject(projectId: Long): List<GitlabProjectDTO> {
        return gitlabProjectRepository.findAllByProjectId(projectId).map(this::getDTO)
    }

    private fun getDTO(it: GitlabProject) =
        GitlabProjectDTO(it.id, it.name, it.lastActivityAt?.format(DateTimeFormatter.ISO_LOCAL_DATE), "${it.lastActivityAt?.until(ZonedDateTime.now(), ChronoUnit.DAYS)} days")

    fun linkProject(id: Long, uuid: UUID): List<GitlabProjectDTO>? {
        gitlabProjectRepository.findByIdOrNull(uuid)?.let {
            it.projectId = id
            gitlabProjectRepository.save(it)
        }
        return getProject(id)
    }

    fun createProject(createGitlabProject: CreateProject, projectId: Long) {
        val project = gitlab4jClient.createProject(createGitlabProject)
        return updateProject(project, projectId)
    }
}
