package cz.cvut.fit.kanakfil.projectdb.gitlab.client

import feign.RequestInterceptor
import feign.RequestTemplate
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration


@Configuration
class GitlabClientConfiguration(
    private val gitlabProperties: GitlabProperties
) {

    @Bean
    fun gitlabTokenInterceptor(): GitlabTokenInterceptor {
        return GitlabTokenInterceptor(gitlabProperties.token)
    }
}

class GitlabTokenInterceptor(private val token: String) : RequestInterceptor {

    override fun apply(template: RequestTemplate?) {
        template?.header("PRIVATE-TOKEN", token)
    }
}

@ConstructorBinding
@ConfigurationProperties(prefix = "modules.config.gitlab")
data class GitlabProperties(val url: String, val token: String, val actions: List<String>)
