package cz.cvut.fit.kanakfil.projectdb.gitlab.model

import java.util.*

data class GitlabProjectDTO(val id: UUID?, val name: String?, val lastActivityAt: String?, val since: String?)
