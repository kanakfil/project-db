package cz.cvut.fit.kanakfil.projectdb.gitlab.client

import cz.cvut.fit.kanakfil.projectdb.gitlab.model.CreateProject
import cz.cvut.fit.kanakfil.projectdb.gitlab.model.ProjectDetail
import org.gitlab4j.api.GitLabApi
import org.springframework.stereotype.Service

@Service
class Gitlab4jClient(gitlabProperties: GitlabProperties, val gitlabFeignClient: GitlabFeignClient) : GitlabClient {

    private val api = GitLabApi(gitlabProperties.url, gitlabProperties.token)

    override fun getProjectDetail(projectId: Long): ProjectDetail {
        return api.projectApi.getProject(projectId).let {
            ProjectDetail(it)
        }
    }

    override fun getNewAndModifiedProjects(activity_after: String): List<ProjectDetail> {
        return gitlabFeignClient.getNewAndModifiedProjects(activity_after)
    }

    override fun createProject(createGitlabProject: CreateProject): ProjectDetail {
        return api.projectApi.createProject(createGitlabProject.name).let { ProjectDetail(it) }
    }
}
