package cz.cvut.fit.kanakfil.projectdb.gitlab.client

import cz.cvut.fit.kanakfil.projectdb.gitlab.model.CreateProject
import cz.cvut.fit.kanakfil.projectdb.gitlab.model.ProjectDetail
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.*

@FeignClient(value = "gitlab-client", url = "\${modules.config.gitlab.url}/api/v4/")
interface GitlabFeignClient : GitlabClient {

    @GetMapping(value = ["/projects/{projectId}?statistics=true"])
    override fun getProjectDetail(@PathVariable("projectId") projectId: Long): ProjectDetail

    @GetMapping(value = ["/projects/"])
    override fun getNewAndModifiedProjects(@RequestParam("last_activity_after") activity_after: String): List<ProjectDetail>

    @PostMapping(value = ["/projects"])
    override fun createProject(@RequestBody createGitlabProject: CreateProject): ProjectDetail

}
