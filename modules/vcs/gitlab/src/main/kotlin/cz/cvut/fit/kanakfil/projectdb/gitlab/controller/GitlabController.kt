package cz.cvut.fit.kanakfil.projectdb.gitlab.controller

import cz.cvut.fit.kanakfil.projectdb.gitlab.model.GitlabProjectDTO
import cz.cvut.fit.kanakfil.projectdb.gitlab.service.GitlabProjectService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException
import java.util.*


@RestController
@RequestMapping("/api/modules/vcs/gitlab")
class GitlabController(val gitlabProjectService: GitlabProjectService) {

    @GetMapping("/projects")
    @PreAuthorize("hasRole('USER')")
    fun getProjects(): ResponseEntity<List<GitlabProjectDTO>> {
        return ResponseEntity.ok(gitlabProjectService.getProjects())
    }

    @GetMapping("/projects/{id}")
    @PreAuthorize("hasRole('USER')")
    fun getProject(@PathVariable id: Long): ResponseEntity<List<GitlabProjectDTO>> {
        return ResponseEntity.ok(gitlabProjectService.getProject(id))
    }

    @PutMapping("/projects/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    fun linkProject(@PathVariable id: Long, @RequestBody gitlabId: Map<String, UUID>): ResponseEntity<List<GitlabProjectDTO>> {
        return ResponseEntity.ok(gitlabProjectService.linkProject(id, gitlabId["gitlabId"] ?: throw ResponseStatusException(HttpStatus.NOT_FOUND)))
    }

}
