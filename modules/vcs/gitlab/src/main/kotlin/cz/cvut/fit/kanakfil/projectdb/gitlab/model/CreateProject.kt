package cz.cvut.fit.kanakfil.projectdb.gitlab.model

data class CreateProject(val name: String)
