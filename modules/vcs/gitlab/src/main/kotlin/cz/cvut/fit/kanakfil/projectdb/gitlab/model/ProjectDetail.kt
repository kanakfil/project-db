package cz.cvut.fit.kanakfil.projectdb.gitlab.model

import org.gitlab4j.api.models.Project
import java.time.ZoneId
import java.time.ZonedDateTime

data class ProjectDetail(val id: Long, val name: String, val last_activity_at: ZonedDateTime) {
    constructor(project: Project) : this(project.id, project.name, project.lastActivityAt.toInstant().atZone(ZoneId.of("UTC")))
}
