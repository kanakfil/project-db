package cz.cvut.fit.kanakfil.projectdb.raynet.domain

import org.hibernate.annotations.GenericGenerator
import org.hibernate.annotations.Type
import java.time.ZonedDateTime
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
class GitlabProject(
    @Column(name = "gitlab_id", nullable = false, unique = true)
    val gitlabId: Long? = null,

    @Column(name = "project_id", unique = false)
    var projectId: Long? = null,

    @Column(name = "name", nullable = false)
    var name: String? = null,

    @Column(name = "last_activity_at")
    var lastActivityAt: ZonedDateTime? = null
) {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "id", nullable = false)
    @Type(type = "uuid-char")
    val id: UUID? = null
}
