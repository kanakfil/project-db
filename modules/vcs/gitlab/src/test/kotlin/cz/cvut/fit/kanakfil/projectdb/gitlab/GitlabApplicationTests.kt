package cz.cvut.fit.kanakfil.projectdb.gitlab

import cz.cvut.fit.kanakfil.projectdb.commons.service.SynchronizationService
import cz.cvut.fit.kanakfil.projectdb.gitlab.client.Gitlab4jClient
import cz.cvut.fit.kanakfil.projectdb.gitlab.client.GitlabFeignClient
import cz.cvut.fit.kanakfil.projectdb.gitlab.model.GitlabProjectDTO
import cz.cvut.fit.kanakfil.projectdb.gitlab.model.ProjectDetail
import cz.cvut.fit.kanakfil.projectdb.gitlab.repository.GitlabProjectRepository
import cz.cvut.fit.kanakfil.projectdb.gitlab.service.GitlabProjectService
import cz.cvut.fit.kanakfil.projectdb.raynet.domain.GitlabProject
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import io.mockk.confirmVerified
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.repository.findByIdOrNull
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.util.*

@SpringBootTest
class GitlabApplicationTests : StringSpec({

    val gitlabFeignClient: GitlabFeignClient = mockk()
    val gitlab4jClient: Gitlab4jClient = mockk()
    val gitlabProjectRepository: GitlabProjectRepository = mockk()
    val synchronizationService: SynchronizationService = mockk(relaxed = true)

    val service = GitlabProjectService(gitlabFeignClient, gitlab4jClient, gitlabProjectRepository, synchronizationService)

    every { gitlabFeignClient.getNewAndModifiedProjects(any()) } returns listOf(ProjectDetail(1, "Coding project", ZonedDateTime.now().minusDays(10)))
    every { gitlabProjectRepository.getByGitlabId(any()) } returns null
    val projectlist = listOf(GitlabProject(1, 1, "Coding project", ZonedDateTime.now().minusDays(10)))
    every { gitlabProjectRepository.findAll() } returns projectlist
    every { gitlabProjectRepository.findAllByProjectId(1) } returns projectlist
    every { gitlabProjectRepository.findByIdOrNull(any()) } returns projectlist.first()
    every { gitlabProjectRepository.save(any()) } returns projectlist.first()


    "Sync new test" {
        service.updateAll()

        verify { synchronizationService.getLastSync(any()) }
        verify { gitlabProjectRepository.getByGitlabId(any()) }
        verify { gitlabFeignClient.getNewAndModifiedProjects(any()) }
        verify { gitlabProjectRepository.save(any()) }

        verify { synchronizationService.syncedNow(any()) }

        confirmVerified(synchronizationService)
        confirmVerified(gitlabProjectRepository)
        confirmVerified(gitlabFeignClient)
    }

    "Get projects" {
        service.getProjects() shouldBe listOf(GitlabProjectDTO(null, "Coding project", ZonedDateTime.now().minusDays(10).format(DateTimeFormatter.ISO_LOCAL_DATE), "10 days"))
        verify { gitlabProjectRepository.findAll() }
        confirmVerified(gitlabProjectRepository)
    }

    "Get projects by project id" {
        service.getProject(1) shouldBe listOf(GitlabProjectDTO(null, "Coding project", ZonedDateTime.now().minusDays(10).format(DateTimeFormatter.ISO_LOCAL_DATE), "10 days"))
        verify { gitlabProjectRepository.findAllByProjectId(any()) }
        confirmVerified(gitlabProjectRepository)
    }

    "Gitlab link project returns DTO" {
        val id = UUID.randomUUID()
        service.linkProject(1, id) shouldBe listOf(GitlabProjectDTO(null, "Coding project", ZonedDateTime.now().minusDays(10).format(DateTimeFormatter.ISO_LOCAL_DATE), "10 days"))
        verify { gitlabProjectRepository.save(any()) }
        verify { gitlabProjectRepository.findByIdOrNull(any()) }
        verify { gitlabProjectRepository.findAllByProjectId(1) }
        confirmVerified(gitlabProjectRepository)
    }

})
