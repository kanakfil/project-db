package cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.model

data class JiraProjectDetail(val key: String, val name: String, val epics: List<EpicDetail>, val worklogs: List<WorklogProjectDetail>, val totals: Map<String, Long>)
data class EpicDetail(val issueId: Int?, val name: String?, val details: String?)
data class WorklogProjectDetail(val worker: String?, val amount: Long?, val month: String?)
