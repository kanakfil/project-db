package cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.repository

import cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.domain.Epic
import cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.domain.JiraProject
import org.springframework.data.repository.CrudRepository
import java.util.*

interface JiraEpicRepository : CrudRepository<Epic, UUID> {
    fun existsByIssueId(issueId: Int): Boolean
    fun findAllByProject(project: JiraProject): List<Epic>
}
