package cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.domain

import org.hibernate.annotations.GenericGenerator
import org.hibernate.annotations.Type
import java.util.*
import javax.persistence.*

@Entity
class Epic(

    @Column(name = "issue_id", nullable = false, unique = true)
    var issueId: Int? = null,

    @Column(name = "name", nullable = false)
    var name: String? = null,

    @Column(name = "details", nullable = true)
    var details: String? = null,

    @ManyToOne
    var project: JiraProject? = null,

    ) {
    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "id", nullable = false)
    @Type(type = "uuid-char")
    val id: UUID? = null
}
