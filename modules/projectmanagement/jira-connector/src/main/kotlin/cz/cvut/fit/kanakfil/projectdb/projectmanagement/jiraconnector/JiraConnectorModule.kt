package cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector

import cz.cvut.fit.kanakfil.projectdb.commons.service.JobScheduler
import cz.cvut.fit.kanakfil.projectdb.commons.service.SchedulerParams
import cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.job.JiraConnectorSyncJob
import cz.cvut.fit.kanakfil.projectdb.util.logger
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component


@Component
@ConditionalOnProperty(name = ["modules.project_management.jira-connector"], havingValue = "enabled")
class RunAfterStartup(private val jobScheduler: JobScheduler) {

    @EventListener(ApplicationReadyEvent::class)
    fun loadModule() {
        logger().info("Loaded module: jira-connector")
        jobScheduler.scheduleJob(
            JiraConnectorSyncJob.JOB_NAME,
            SchedulerParams(21, 35),
            JiraConnectorSyncJob::class.java
        )
    }
}

