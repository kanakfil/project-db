package cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.client

import cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.model.*
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RequestParam


@FeignClient(value = "jira-connector-client", url = "\${modules.config.jira-connector.url}")
interface JiraConnectorClient {

    @GetMapping(value = ["/worklogSummaries"])
    fun getWorklogSummaries(
        @RequestParam("month") month: String,
        @RequestParam("users") users: String = "activeQuanti",
        @RequestParam("perProjectDetails") perProjectDetails: Boolean = true,
        @RequestHeader(X_API_TOKEN) token: String
    ): List<WorklogSummary>

    @GetMapping(value = ["/projects"])
    fun getProjects(
        @RequestParam("page") page: Int,
        @RequestParam("size") size: Int,
        @RequestHeader(X_API_TOKEN) token: String
    ): Paginable<JiraProjectDto>

    @GetMapping(value = ["/epics"])
    fun getEpics(
        @RequestParam("page") page: Int,
        @RequestParam("size") size: Int,
        @RequestHeader(X_API_TOKEN) token: String
    ): Paginable<EpicDto>


    @PostMapping(value = ["/auth"])
    fun auth(auth: Auth): TokenResponse

    companion object {
        const val X_API_TOKEN = "X-Api-token"
    }
}
