package cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.controller

import cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.domain.JiraProject
import cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.model.JiraId
import cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.service.JiraService
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*


@RestController
@RequestMapping("/api/modules/projectmanagement/jira")
class JiraController(val jiraService: JiraService) {

    @GetMapping("/projects")
    @PreAuthorize("hasRole('ADMIN')")
    fun getProjects(): ResponseEntity<List<JiraProject>> {
        return ResponseEntity.ok(jiraService.getProjects())
    }

    @GetMapping("/projects/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    fun getProject(@PathVariable id: Long): ResponseEntity<Map<String, Any>> {
        return ResponseEntity.ok(jiraService.getProjectDetails(id))
    }

    @PutMapping("/projects/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    fun linkProject(@PathVariable id: Long, @RequestBody jiraId: JiraId): ResponseEntity<Map<String, Any>> {
        return ResponseEntity.ok(jiraService.linkProject(id, jiraId.jiraId))
    }

}
