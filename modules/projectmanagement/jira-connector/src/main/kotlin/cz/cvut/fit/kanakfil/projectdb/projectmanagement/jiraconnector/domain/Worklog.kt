package cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.domain

import cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.domain.Worklog.Companion.MONTH_COLUMN_NAME
import cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.domain.Worklog.Companion.WORKER_COLUMN_NAME
import javax.persistence.*

@Entity
@Table(uniqueConstraints = [UniqueConstraint(columnNames = [MONTH_COLUMN_NAME, WORKER_COLUMN_NAME])])
class Worklog(

    @ManyToOne
    @JoinColumn(name = WORKER_COLUMN_NAME, nullable = false)
    val worker: Worker? = null,

    @Column(name = MONTH_COLUMN_NAME, nullable = false)
    val month: String? = null,

    @Column(name = "amount", nullable = false)
    var amount: Long? = null,
) {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    var id: Long? = null

    @OneToMany(mappedBy = "worklog", cascade = [CascadeType.ALL], fetch = FetchType.EAGER)
    var projects: List<WorklogProject> = listOf()


    companion object {
        const val MONTH_COLUMN_NAME = "month"
        const val WORKER_COLUMN_NAME = "worker_id"
    }
}
