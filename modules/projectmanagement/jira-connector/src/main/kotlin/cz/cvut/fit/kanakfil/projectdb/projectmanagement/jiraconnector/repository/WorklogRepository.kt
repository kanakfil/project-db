package cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.repository

import cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.domain.JiraProject
import cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.domain.Worker
import cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.domain.Worklog
import cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.domain.WorklogProject
import org.springframework.data.repository.CrudRepository
import java.util.*

interface WorklogRepository : CrudRepository<Worklog, UUID> {
    fun findByWorkerAndMonth(worker: Worker, month: String): Worklog?
    fun findAllByProjectsContainingAndMonth(project: JiraProject, month: String): List<Worklog>
}

interface  WorklogProjectRepository : CrudRepository<WorklogProject, Long>{
    fun findAllByProject(project: JiraProject): List<WorklogProject>
}
