package cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.config

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import org.springframework.context.annotation.Configuration

@Configuration
@ConditionalOnProperty(name = ["modules.projectmanagement"], havingValue = "jira-connector")
class JiraConnectorConfiguration {
}

@ConstructorBinding
@ConfigurationProperties(prefix = "modules.config.jira-connector")
data class JiraConnectorProperties(val username: String, val password: String)
