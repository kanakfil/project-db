package cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.model

import java.util.UUID

data class JiraId(
    val jiraId: UUID
)
