package cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.model

data class EpicDto(
    val issueId: Int,
    val project: JiraProjectDto,
    val name: String,
    val details: String?,
    val projectKey: String
)
