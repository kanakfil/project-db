package cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.model

data class Paginable<T>(val items: List<T>, val metadata: Meta)
data class Meta(val itemCount: Int, val page: Int, val pageCount: Int, val size: Int)
