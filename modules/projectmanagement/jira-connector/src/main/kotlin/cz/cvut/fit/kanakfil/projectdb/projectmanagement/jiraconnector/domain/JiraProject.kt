package cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.domain

import org.hibernate.annotations.GenericGenerator
import org.hibernate.annotations.Type
import java.util.*
import javax.persistence.*

@Entity
class JiraProject(
    @Column(name = "jiraKey", nullable = false, unique = true)
    val key: String? = null,

    @Column(name = "name", nullable = false)
    val name: String? = null,

    @Column(name = "project_id", unique = false)
    var projectId: Long? = null,

    @OneToMany
    val epics: List<Epic> = listOf()
) {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "id", nullable = false)
    @Type(type = "uuid-char")
    val id: UUID? = null
}
