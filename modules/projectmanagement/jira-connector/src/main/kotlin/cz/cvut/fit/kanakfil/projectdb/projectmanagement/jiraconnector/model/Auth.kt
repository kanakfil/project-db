package cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.model

import java.time.ZonedDateTime

data class Auth(val username: String, val password: String)
data class TokenResponse(val token: String, val username: String, val expiresOn: ZonedDateTime)
