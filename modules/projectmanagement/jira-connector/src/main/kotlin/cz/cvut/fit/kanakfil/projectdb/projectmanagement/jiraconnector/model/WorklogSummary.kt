package cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.model

import java.util.*

data class WorklogSummary(val timeSpentSeconds: Long, val worker: WorkerDto, val projects: Map<String, Long> = mapOf())
data class WorkerDto(val uuid: UUID, val username: String, val emailAddress: String, val displayName: String)
