package cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.model

data class JiraProjectDto(val key: String, val name: String)
