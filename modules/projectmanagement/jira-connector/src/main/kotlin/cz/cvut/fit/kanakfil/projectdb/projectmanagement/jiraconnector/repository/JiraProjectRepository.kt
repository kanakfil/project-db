package cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.repository

import cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.domain.JiraProject
import org.springframework.data.repository.CrudRepository
import java.util.*

interface JiraProjectRepository : CrudRepository<JiraProject, UUID> {
    fun existsByKey(key: String): Boolean
    fun findByKey(key: String): JiraProject?
    fun findAllByProjectId(id: Long) : List<JiraProject>
}
