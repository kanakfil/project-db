package cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.service

import cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.domain.JiraProject
import cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.model.EpicDetail
import cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.model.JiraProjectDetail
import cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.model.WorklogProjectDetail
import cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.repository.JiraEpicRepository
import cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.repository.JiraProjectRepository
import cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.repository.WorklogProjectRepository
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import java.util.*

@Service
class JiraService(
    private val jiraProjectRepository: JiraProjectRepository, private val worklogProjectRepository: WorklogProjectRepository, private val jiraEpicRepository: JiraEpicRepository
) {

    fun getProjects(): List<JiraProject> {
        return jiraProjectRepository.findAll().toList()
    }

    fun getProjectDetails(id: Long): Map<String, Any> {
        val details = jiraProjectRepository.findAllByProjectId(id).map {
            val worklogs = worklogProjectRepository.findAllByProject(it).map { wp ->
                WorklogProjectDetail(wp.worklog?.worker?.displayName, wp.amount, wp.worklog?.month)
            }
            val epics = jiraEpicRepository.findAllByProject(it).map { e ->
                EpicDetail(e.issueId, e.name, e.details)
            }
            val totals = worklogs.groupBy { w -> w.month ?: "" }.mapValues { v -> v.value.sumOf { wl -> wl.amount ?: 0 } }
            return@map JiraProjectDetail(it.key!!, it.name!!, epics, worklogs, totals)
        }
        val totals = details.map { it.totals }.reduceOrNull { acc, next -> (acc.asSequence() + next.asSequence()).groupBy({ it.key }, { it.value }).mapValues { (_, values) -> values.sum() } } ?: mapOf()
        return mapOf("details" to details, "totals" to totals)
    }

    fun linkProject(id: Long, jiraId: UUID): Map<String, Any> {
        jiraProjectRepository.findByIdOrNull(jiraId)?.let {
            it.projectId = id
            jiraProjectRepository.save(it)
        }
        return getProjectDetails(id)
    }
}
