package cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.job

import cz.cvut.fit.kanakfil.projectdb.commons.service.JobData
import cz.cvut.fit.kanakfil.projectdb.commons.service.JobScheduler
import cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.service.SyncService
import cz.cvut.fit.kanakfil.projectdb.util.logger
import org.quartz.Job
import org.quartz.JobExecutionContext
import org.quartz.JobExecutionException
import org.springframework.stereotype.Component
import java.lang.String.format

@Component
class JiraConnectorSyncJob(val syncService: SyncService) : Job {

    @Throws(JobExecutionException::class)
    override fun execute(jobExecutionContext: JobExecutionContext) {
        val jobDataMap = jobExecutionContext.mergedJobDataMap
        val refireCount = jobExecutionContext.refireCount
        val jobData = JobData(jobDataMap.getString(JobScheduler.JOB_NAME_KEY))
        try {
            syncService.sync()
            if (refireCount > MAX_RETRIES) {
                throw JobExecutionException(
                    format(
                        "Job execution retries exceeded for job name %s",
                        jobData.jobName
                    )
                )
            }
            logger().info("Job execution for name: {}", jobData.jobName)
        } catch (e: Exception) {
            val jobExecutionException = JobExecutionException(e)

            // fire it again
            jobExecutionException.setRefireImmediately(refireCount <= MAX_RETRIES)
            throw jobExecutionException
        }
    }

    companion object {
        const val JOB_NAME = "jira_connector_sync"
        private const val MAX_RETRIES = 3
    }
}
