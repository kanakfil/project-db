package cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.domain

import cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.domain.WorklogProject.Companion.JIRA_PROJECT_COLUMN_NAME
import cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.domain.WorklogProject.Companion.WORKLOG_COLUMN_NAME
import javax.persistence.*

@Entity
@Table(uniqueConstraints = [UniqueConstraint(columnNames = [WORKLOG_COLUMN_NAME, JIRA_PROJECT_COLUMN_NAME])])
class WorklogProject(

    @ManyToOne
    @JoinColumn(name = WORKLOG_COLUMN_NAME, nullable = false)
    val worklog: Worklog? = null,

    @ManyToOne
    @JoinColumn(name = JIRA_PROJECT_COLUMN_NAME, nullable = false)
    val project: JiraProject? = null,

    @Column
    var amount: Long? = null
) {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    var id: Long? = null

    companion object {
        const val WORKLOG_COLUMN_NAME = "worklog_id"
        const val JIRA_PROJECT_COLUMN_NAME = "jira_project_id"
    }
}
