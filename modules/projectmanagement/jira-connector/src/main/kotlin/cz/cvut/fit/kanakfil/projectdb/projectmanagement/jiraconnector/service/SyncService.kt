package cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.service

import cz.cvut.fit.kanakfil.projectdb.commons.domain.SynchronizationLog
import cz.cvut.fit.kanakfil.projectdb.commons.service.SynchronizationService
import cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.client.JiraConnectorClient
import cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.config.JiraConnectorProperties
import cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.domain.*
import cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.model.Auth
import cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.repository.JiraEpicRepository
import cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.repository.JiraProjectRepository
import cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.repository.WorkerRepository
import cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.repository.WorklogRepository
import org.springframework.stereotype.Service
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

@Service
class SyncService(
    val client: JiraConnectorClient,
    val properties: JiraConnectorProperties,
    val jiraProjectRepository: JiraProjectRepository,
    val jiraEpicRepository: JiraEpicRepository,
    val worklogRepository: WorklogRepository,
    val workerRepository: WorkerRepository,
    val synchronizationService: SynchronizationService
) {
    fun sync() {
        val token = client.auth(Auth(properties.username, properties.password)).token

        syncProjects(token)
        syncEpics(token)
        syncWorklogs(token)

        synchronizationService.syncedNow(MODULE_NAME)
    }

    private fun syncWorklogs(token: String) {
        val lastSync = synchronizationService.getLastSync(MODULE_NAME)
        if (lastSync.lastSync.month != LocalDateTime.now().month) {
            syncHistory(lastSync, token)
        } else {
            syncMonth(LocalDateTime.now(), token)
        }
    }

    private fun syncMonth(dateTime: LocalDateTime, token: String): Int {
        val month = dateTime.format(DateTimeFormatter.ofPattern("YYYY-MM"))
        return client.getWorklogSummaries(
            month,
            token = token
        ).map { worklogSummary ->
            val worker = with(worklogSummary.worker) {
                workerRepository.findById(uuid).orElse(
                    workerRepository.save(Worker(uuid, username, emailAddress, displayName))
                )
            }

            val worklog = (worklogRepository.findByWorkerAndMonth(
                worker,
                month
            ) ?: Worklog(worker, month)).apply { amount = worklogSummary.timeSpentSeconds }

            val projects =
                worklogSummary.projects.filter { wsp -> worklog.projects.all { wp -> wp.project?.key != wsp.key } }
                    .map {
                        WorklogProject(
                            worklog,
                            jiraProjectRepository.findByKey(it.key),
                            it.value
                        )
                    }
            worklog.projects = worklog.projects.onEach {
                worklogSummary.projects[it.project!!.key!!].let { time ->
                    it.amount = time
                }
            } + projects
            worklog
        }.apply(worklogRepository::saveAll).count()
    }

    private fun syncHistory(lastSync: SynchronizationLog?, token: String) {
        var month = LocalDateTime.now()
        do {
            month = month.minusMonths(1)
            val count = syncMonth(month, token)
        } while ((month > ((lastSync?.lastSync) ?: LocalDateTime.MIN)) && count > 0)
    }

    private fun syncEpics(token: String) {
        var page = 0
        do {
            val projects = client.getEpics(page, 50, token)
            val newEpics = projects.items.filterNot { jiraEpicRepository.existsByIssueId(it.issueId) }.map {
                val project = jiraProjectRepository.findByKey(it.projectKey)
                Epic(it.issueId, it.name, it.details, project)
            }
            jiraEpicRepository.saveAll(newEpics)
            page++
        } while (projects.metadata.pageCount > page)
    }

    private fun syncProjects(token: String) {
        var page = 0
        do {
            val projects = client.getProjects(page, 50, token)
            val newProjects = projects.items.filterNot { jiraProjectRepository.existsByKey(it.key) }.map {
                JiraProject(it.key, it.name)
            }
            jiraProjectRepository.saveAll(newProjects)
            page++
        } while (projects.metadata.pageCount > page)
    }

    companion object {
        const val MODULE_NAME = "JIRA_CONNECTOR"
    }
}
