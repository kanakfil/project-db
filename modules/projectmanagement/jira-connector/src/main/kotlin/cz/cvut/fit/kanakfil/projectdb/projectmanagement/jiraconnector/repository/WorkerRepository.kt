package cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.repository

import cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.domain.Worker
import org.springframework.data.repository.CrudRepository
import java.util.*

interface WorkerRepository : CrudRepository<Worker, UUID> {
}
