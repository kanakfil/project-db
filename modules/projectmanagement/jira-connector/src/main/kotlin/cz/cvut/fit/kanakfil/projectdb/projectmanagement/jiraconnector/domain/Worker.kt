package cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.domain

import org.hibernate.annotations.Type
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id

@Entity
class Worker(

    @Id
    @Column(name = "id", nullable = false)
    @Type(type = "uuid-char")
    val uuid: UUID? = null,

    @Column(name = "username", nullable = false)
    val username: String? = null,

    @Column(name = "emailAddress", nullable = true)
    val emailAddress: String? = null,

    @Column(name = "displayName", nullable = true)
    val displayName: String? = null

)
