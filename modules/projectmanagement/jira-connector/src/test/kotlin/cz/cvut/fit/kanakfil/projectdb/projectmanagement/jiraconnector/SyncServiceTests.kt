package cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector

import cz.cvut.fit.kanakfil.projectdb.commons.domain.SynchronizationLog
import cz.cvut.fit.kanakfil.projectdb.commons.service.SynchronizationService
import cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.client.JiraConnectorClient
import cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.config.JiraConnectorProperties
import cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.domain.Epic
import cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.domain.JiraProject
import cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.domain.Worker
import cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.domain.Worklog
import cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.model.*
import cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.repository.JiraEpicRepository
import cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.repository.JiraProjectRepository
import cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.repository.WorkerRepository
import cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.repository.WorklogRepository
import cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.service.SyncService
import io.kotest.core.spec.style.StringSpec
import io.mockk.confirmVerified
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import java.time.LocalDateTime
import java.time.ZonedDateTime
import java.util.*

class SyncServiceTests : StringSpec({
    val client: JiraConnectorClient = mockk(relaxed = true)
    val properties: JiraConnectorProperties = mockk(relaxed = true)
    val jiraProjectRepository: JiraProjectRepository = mockk(relaxed = true)
    val jiraEpicRepository: JiraEpicRepository = mockk(relaxed = true)
    val worklogRepository: WorklogRepository = mockk(relaxed = true)
    val workerRepository: WorkerRepository = mockk(relaxed = true)
    val synchronizationService: SynchronizationService = mockk(relaxed = true)
    val service = SyncService(client, properties, jiraProjectRepository, jiraEpicRepository, worklogRepository, workerRepository, synchronizationService)

    val project = JiraProjectDto("TEST", "Testing project")

    every { client.auth(any()) } returns TokenResponse("token", "", ZonedDateTime.now())
    every { client.getProjects(any(), any(), any()) } returns Paginable(listOf(project), Meta(1, 1, 1, 1))
    every { client.getEpics(any(), any(), any()) } returns Paginable(listOf(EpicDto(123, project, "Testing", "Testing of projectsdb application", "TEST")), Meta(1, 1, 1, 1))
    every { client.getWorklogSummaries(any(), any(), any(), any()) } returns listOf(WorklogSummary(1000, WorkerDto(UUID.randomUUID(), "worker1", "email@example.com", "Worker One"), mapOf("TEST" to 1000)))
    every { synchronizationService.getLastSync(any()) } returns SynchronizationLog("jiraTest", LocalDateTime.now().minusMonths(1))

    every { jiraProjectRepository.existsByKey(any()) } returns true
    every { jiraProjectRepository.findByKey(any()) } returns JiraProject()
    every { jiraEpicRepository.existsByIssueId(any()) } returns true
    every { workerRepository.findById(any()) } returns Optional.empty()
    every { workerRepository.save(any()) } returns Worker()
    every { worklogRepository.findByWorkerAndMonth(any(), any()) } returns null

    "Verify sync" {
        service.sync()

        verify { synchronizationService.getLastSync(any()) }

        //syncProjects
        verify { jiraProjectRepository.existsByKey(any()) }
        verify { jiraProjectRepository.saveAll(any<List<JiraProject>>()) }

        //syncEpics
        verify { jiraEpicRepository.existsByIssueId(any()) }
        verify { jiraEpicRepository.saveAll(any<List<Epic>>()) }
        verify { jiraProjectRepository.findByKey(any()) }

        //syncWorklogs
        verify { workerRepository.findById(any()) }
        verify { workerRepository.save(any()) }
        verify { worklogRepository.findByWorkerAndMonth(any(), any()) }
        verify { worklogRepository.saveAll(any<List<Worklog>>()) }

        verify { synchronizationService.syncedNow(any()) }

        confirmVerified(jiraProjectRepository)
        confirmVerified(synchronizationService)
        confirmVerified(worklogRepository)
        confirmVerified(workerRepository)
        confirmVerified(jiraEpicRepository)

    }
})
