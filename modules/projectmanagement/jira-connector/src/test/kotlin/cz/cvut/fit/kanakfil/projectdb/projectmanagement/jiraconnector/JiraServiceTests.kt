package cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector

import cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.domain.*
import cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.model.EpicDetail
import cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.model.JiraProjectDetail
import cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.model.WorklogProjectDetail
import cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.repository.JiraEpicRepository
import cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.repository.JiraProjectRepository
import cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.repository.WorklogProjectRepository
import cz.cvut.fit.kanakfil.projectdb.projectmanagement.jiraconnector.service.JiraService
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import io.mockk.confirmVerified
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.springframework.data.repository.findByIdOrNull
import java.util.*

class JiraServiceTests : StringSpec({

    val jiraProjectRepository = mockk<JiraProjectRepository>(relaxed = true)
    val worklogProjectRepository = mockk<WorklogProjectRepository>()
    val jiraEpicRepository = mockk<JiraEpicRepository>()
    val service = JiraService(jiraProjectRepository, worklogProjectRepository, jiraEpicRepository)

    val epic = Epic(123, "Initial epic", "Epic made for testing")
    val project = JiraProject("TEST", "Testing project", 1, listOf())
    val second = JiraProject("TEST2", "Second testing project", 1, listOf())
    epic.project = project

    every { jiraProjectRepository.findAll() } returns listOf(project, second)
    every { jiraProjectRepository.findAllByProjectId(1) } returns listOf(project, second)
    every { jiraProjectRepository.findByIdOrNull(any()) } returns project
    every { jiraProjectRepository.save(eq(project)) } returns project
    every { jiraEpicRepository.findAllByProject(eq(project)) } returns listOf(epic)
    every { jiraEpicRepository.findAllByProject(neq(project)) } returns listOf()
    every { worklogProjectRepository.findAllByProject(any()) } returns listOf(WorklogProject(Worklog(Worker(UUID.randomUUID(), "tester", "tester@example.com", "Tester"), "2022-04", 1000), project, 1000))


    "Jira projects test" {
        service.getProjects() shouldBe listOf(project, second)
        verify { jiraProjectRepository.findAll() }
        confirmVerified(jiraProjectRepository)
    }

    "Jira project DTO totals" {
        service.getProjectDetails(1) shouldBe mapOf(
            "details" to listOf(
                JiraProjectDetail("TEST", "Testing project", listOf(EpicDetail(123, "Initial epic", "Epic made for testing")), listOf(WorklogProjectDetail("Tester", 1000, "2022-04")), mapOf("2022-04" to 1000)),
                JiraProjectDetail(
                    "TEST2", "Second testing project", listOf(), listOf(WorklogProjectDetail("Tester", 1000, "2022-04")), mapOf("2022-04" to 1000)
                )
            ),
            "totals" to mapOf("2022-04" to 2000)
        )
        verify { jiraProjectRepository.findAllByProjectId(1) }
        confirmVerified(jiraProjectRepository)
    }

    "Jira link project returns DTO" {
        val id = UUID.randomUUID()
        service.linkProject(1, id) shouldBe mapOf(
            "details" to listOf(
                JiraProjectDetail("TEST", "Testing project", listOf(EpicDetail(123, "Initial epic", "Epic made for testing")), listOf(WorklogProjectDetail("Tester", 1000, "2022-04")), mapOf("2022-04" to 1000)),
                JiraProjectDetail(
                    "TEST2", "Second testing project", listOf(), listOf(WorklogProjectDetail("Tester", 1000, "2022-04")), mapOf("2022-04" to 1000)
                )
            ),
            "totals" to mapOf("2022-04" to 2000)
        )
        verify { jiraProjectRepository.save(project) }
        verify { jiraProjectRepository.findByIdOrNull(any()) }
        verify { jiraProjectRepository.findAllByProjectId(1) }
        confirmVerified(jiraProjectRepository)
    }

})
