package cz.cvut.fit.kanakfil.projectdb.commons.repository

import cz.cvut.fit.kanakfil.projectdb.commons.domain.SynchronizationLog
import org.apache.commons.logging.Log
import org.springframework.data.repository.CrudRepository

interface SynchronizationRepository : CrudRepository<SynchronizationLog, Log> {
    fun getFirstByModuleOrderByLastSyncDesc(module: String): SynchronizationLog?
}
