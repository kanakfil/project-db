package cz.cvut.fit.kanakfil.projectdb.commons.domain

import javax.persistence.*

@Entity
class ProjectType(
    @Column(name = "name", nullable = false)
    val name: String,
) {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    var id: Long? = null
}
