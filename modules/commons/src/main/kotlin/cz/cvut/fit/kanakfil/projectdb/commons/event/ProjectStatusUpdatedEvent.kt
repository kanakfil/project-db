package cz.cvut.fit.kanakfil.projectdb.commons.event

import cz.cvut.fit.kanakfil.projectdb.commons.domain.Action

class ProjectStatusUpdatedEvent(val projectId: Long, val projectName: String, val action: Action) {
}
