package cz.cvut.fit.kanakfil.projectdb.commons.service

import cz.cvut.fit.kanakfil.projectdb.util.logger
import org.quartz.*
import org.springframework.stereotype.Service
import java.time.ZonedDateTime
import java.util.*

@Service
class JobScheduler(private val scheduler: Scheduler) {

    @Throws(SchedulerException::class)
    fun scheduleJob(jobName: String, schedulerParams: SchedulerParams, job: Class<out Job>): TriggerKey {
        val jobKey = JobKey.jobKey(jobName, MY_JOB_GROUP)
        val jobData = JobData(jobName)
        if (scheduler.checkExists(jobKey)) {
            logger().error("job $ exists", jobKey.toString())
            throw JobAlreadyExistsException(jobName)
        }
        logger().info(
            "Updating job for name {} and time {}",
            jobName,
            schedulerParams.hour.toString() + ":" + schedulerParams.minute
        )
        val jobDataMap: JobDataMap = getJobDataMap(jobData)
        val jobDetail: JobDetail = getJobDetail(jobKey, jobDataMap, job)
        val trigger: Trigger = getDailyTrigger(schedulerParams.hour, schedulerParams.minute)
        scheduler.scheduleJob(jobDetail, trigger)
        return trigger.key
    }

    private fun getTrigger(jobDateTime: ZonedDateTime): Trigger {
        return TriggerBuilder.newTrigger()
            .withIdentity(UUID.randomUUID().toString(), MY_JOB_GROUP)
            .startAt(Date.from(jobDateTime.toInstant()))
            .build()
    }

    private fun getDailyTrigger(hour: Int, minute: Int): Trigger {
        return TriggerBuilder.newTrigger()
            .withIdentity(UUID.randomUUID().toString(), MY_JOB_GROUP)
            .withSchedule(CronScheduleBuilder.dailyAtHourAndMinute(hour, minute))
            .build()
    }

    private fun getJobDetail(jobKey: JobKey, jobDataMap: JobDataMap, job: Class<out Job>): JobDetail {
        return JobBuilder.newJob(job)
            .withIdentity(jobKey)
            .usingJobData(jobDataMap)
            .requestRecovery(true)
            .build()
    }

    private fun getJobDataMap(jobData: JobData): JobDataMap {
        val jobDataMap = JobDataMap()
        jobDataMap[JOB_NAME_KEY] = jobData.jobName
        return jobDataMap
    }

    companion object {
        private const val MY_JOB_GROUP = "projectsdb"
        const val JOB_NAME_KEY = "job_name"
    }

}

class JobAlreadyExistsException(jobName: String?) : Throwable()
data class JobData(val jobName: String)
data class SchedulerParams(val hour: Int, val minute: Int)
