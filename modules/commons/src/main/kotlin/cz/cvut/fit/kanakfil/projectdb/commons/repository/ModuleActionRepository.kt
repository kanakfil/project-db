package cz.cvut.fit.kanakfil.projectdb.commons.repository;

import cz.cvut.fit.kanakfil.projectdb.commons.domain.ModuleAction
import org.springframework.data.repository.CrudRepository

interface ModuleActionRepository : CrudRepository<ModuleAction, Long> {
    fun existsByModuleAndAction(module: String, action: String): Boolean
    fun findByModuleAndAction(module: String, action: String): ModuleAction
}
