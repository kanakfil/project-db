package cz.cvut.fit.kanakfil.projectdb.commons.domain

import javax.persistence.*

@Entity
class ProjectStatus(
    @Column(name = "name", nullable = false) val name: String,
    @ManyToMany(mappedBy = "projectStatuses", fetch = FetchType.EAGER) val models: List<LifecycleModel>
) {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    var id: Long? = null

}
