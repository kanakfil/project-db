package cz.cvut.fit.kanakfil.projectdb.commons.event

import java.util.*

data class ProjectCreatedEvent(val id: UUID?, val projectId: Long)
