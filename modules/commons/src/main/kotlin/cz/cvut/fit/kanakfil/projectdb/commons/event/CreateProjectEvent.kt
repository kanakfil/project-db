package cz.cvut.fit.kanakfil.projectdb.commons.event

import cz.cvut.fit.kanakfil.projectdb.commons.domain.ProjectStatus
import java.util.*

data class CreateProjectEvent(val id: UUID?, val name: String, val status: ProjectStatus?) {
    constructor(id: UUID, name: String) : this(id, name, null)
}
