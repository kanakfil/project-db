package cz.cvut.fit.kanakfil.projectdb.commons.event

import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonSubTypes.Type
import com.fasterxml.jackson.annotation.JsonTypeInfo

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes(
    Type(value = UpdateProjectStatusEvent::class, name = "status"),
    Type(value = UpdateProjectTypeEvent::class, name = "type"),
    Type(value = UpdateProjectModelEvent::class, name = "model")
)
interface UpdateProjectEvent {
    fun process(visitor: UpdateEventVisitor, projectId: Long)
}

data class UpdateProjectStatusEvent(val from: String, val to: String) : UpdateProjectEvent {
    override fun process(visitor: UpdateEventVisitor, projectId: Long) {
        visitor.visit(this, projectId)
    }
}

data class UpdateProjectTypeEvent(val to: String) : UpdateProjectEvent {
    override fun process(visitor: UpdateEventVisitor, projectId: Long) {
        visitor.visit(this, projectId)
    }
}

data class UpdateProjectModelEvent(val to: String) : UpdateProjectEvent {
    override fun process(visitor: UpdateEventVisitor, projectId: Long) {
        visitor.visit(this, projectId)
    }
}

interface UpdateEventVisitor {
    fun visit(updateProjectStatusEvent: UpdateProjectStatusEvent, projectId: Long)
    fun visit(updateProjectStatusEvent: UpdateProjectTypeEvent, projectId: Long)
    fun visit(updateProjectStatusEvent: UpdateProjectModelEvent, projectId: Long)
}
