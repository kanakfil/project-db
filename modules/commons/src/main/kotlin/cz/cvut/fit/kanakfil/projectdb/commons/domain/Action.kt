package cz.cvut.fit.kanakfil.projectdb.commons.domain

import cz.cvut.fit.kanakfil.projectdb.commons.domain.Action.Companion.FROM_COLUMN_NAME
import cz.cvut.fit.kanakfil.projectdb.commons.domain.Action.Companion.TO_COLUMN_NAME
import javax.persistence.*

@Entity
@Table(uniqueConstraints = [UniqueConstraint(columnNames = [FROM_COLUMN_NAME, TO_COLUMN_NAME])])
class Action(

    @ManyToOne
    @JoinColumn(name = FROM_COLUMN_NAME)
    val from: ProjectStatus,

    @ManyToOne
    @JoinColumn(name = TO_COLUMN_NAME)
    val to: ProjectStatus,

    @ManyToMany
    val moduleActions: List<ModuleAction>
) {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    var id: Long? = null

    companion object {
        const val FROM_COLUMN_NAME: String = "project_status_from"
        const val TO_COLUMN_NAME: String = "project_status_to"
    }
}
