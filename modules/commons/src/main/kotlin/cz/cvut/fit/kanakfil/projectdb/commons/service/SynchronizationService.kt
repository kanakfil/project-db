package cz.cvut.fit.kanakfil.projectdb.commons.service

import cz.cvut.fit.kanakfil.projectdb.commons.domain.SynchronizationLog
import cz.cvut.fit.kanakfil.projectdb.commons.repository.SynchronizationRepository
import org.springframework.stereotype.Service
import java.time.LocalDateTime
import java.time.ZoneOffset

@Service
class SynchronizationService(private val synchronizationRepository: SynchronizationRepository) {

    fun getLastSync(module: String): SynchronizationLog {
        return synchronizationRepository.getFirstByModuleOrderByLastSyncDesc(module) ?: SynchronizationLog(
            module, LocalDateTime.ofEpochSecond(1, 1, ZoneOffset.UTC)
        )
    }

    fun syncedNow(module: String) {
        synchronizationRepository.save(SynchronizationLog(module, LocalDateTime.now()))
    }

}
