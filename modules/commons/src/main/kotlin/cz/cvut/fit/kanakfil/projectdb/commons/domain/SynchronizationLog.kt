package cz.cvut.fit.kanakfil.projectdb.commons.domain

import java.time.LocalDateTime
import javax.persistence.*

@Entity
class SynchronizationLog(
    @Column(name = "module", nullable = false) val module: String,
    @Column(name = "last_sync", nullable = false) val lastSync: LocalDateTime,
) {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    var id: Long? = null


}
