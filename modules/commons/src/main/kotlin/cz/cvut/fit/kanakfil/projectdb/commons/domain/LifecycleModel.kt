package cz.cvut.fit.kanakfil.projectdb.commons.domain

import javax.persistence.*

@Entity
class LifecycleModel(
    @Column(name = "name", nullable = false)
    val name: String,

    @ManyToMany(cascade = [CascadeType.PERSIST], fetch = FetchType.EAGER)
    @JoinTable(name = "lifecycle_status")
    val projectStatuses: List<ProjectStatus>
) {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    var id: Long? = null
}
