package cz.cvut.fit.kanakfil.projectdb.commons.domain

import javax.persistence.*

@Entity
class ModuleAction(val module: String, val action: String) {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    var id: Long? = null
}
