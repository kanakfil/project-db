package cz.cvut.fit.kanakfil.projectdb.commons

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class CommonsApplicationTests : StringSpec(
    {

        "Test configuration should work" {
            true shouldBe true
        }

    }
)
