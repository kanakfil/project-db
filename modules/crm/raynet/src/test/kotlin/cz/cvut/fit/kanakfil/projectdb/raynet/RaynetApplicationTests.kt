package cz.cvut.fit.kanakfil.projectdb.raynet

import cz.cvut.fit.kanakfil.projectdb.commons.domain.SynchronizationLog
import cz.cvut.fit.kanakfil.projectdb.commons.event.CreateProjectEvent
import cz.cvut.fit.kanakfil.projectdb.commons.service.SynchronizationService
import cz.cvut.fit.kanakfil.projectdb.raynet.client.RaynetCrmClient
import cz.cvut.fit.kanakfil.projectdb.raynet.client.RaynetProperties
import cz.cvut.fit.kanakfil.projectdb.raynet.domain.RaynetProject
import cz.cvut.fit.kanakfil.projectdb.raynet.model.*
import cz.cvut.fit.kanakfil.projectdb.raynet.repository.RaynetProjectRepository
import cz.cvut.fit.kanakfil.projectdb.raynet.service.RaynetProjectService
import io.kotest.core.spec.style.StringSpec
import io.mockk.*
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.ApplicationEventPublisher
import java.time.LocalDateTime
import java.time.ZonedDateTime
import java.util.*

@SpringBootTest
class RaynetApplicationTests : StringSpec({

    val raynetCrmClient: RaynetCrmClient = mockk()
    val raynetProjectRepository: RaynetProjectRepository = mockk(relaxed = true)
    val synchronizationService: SynchronizationService = mockk(relaxed = true)
    val applicationEventPublisher: ApplicationEventPublisher = mockk(relaxed = true)
    val raynetProperties: RaynetProperties = mockk()
    val service = RaynetProjectService(raynetCrmClient, raynetProjectRepository, synchronizationService, applicationEventPublisher, raynetProperties)

    every { synchronizationService.getLastSync(any()) } returns SynchronizationLog("raynetTest", LocalDateTime.now().minusMonths(1))

    val project = ProjectDetailData(1, ProjectStatus(7, "ORDER_ACCEPTED"), "Testing project")
    every { raynetCrmClient.getNewAndModifiedProjects(any()) } returns ProjectList(listOf(project))
    every { raynetCrmClient.getProjectDetail(any()) } returns ProjectDetail(project)


    val projectCaptureSlot = slot<RaynetProject>()
    val uuid = UUID.randomUUID()
    every { raynetProjectRepository.save(capture(projectCaptureSlot)) } answers {
        projectCaptureSlot.captured.apply {
            id = uuid
        }
    }
    every { raynetProjectRepository.getByRaynetId(any()) } returns null
    every { raynetProperties.createProjectStatus } returns 7

    "Sync new test" {
        service.updateAll()

        verify { synchronizationService.getLastSync(any()) }
        verify { raynetProjectRepository.getByRaynetId(any()) }
        verify { raynetProjectRepository.save(any()) }

        verify { applicationEventPublisher.publishEvent(eq(CreateProjectEvent(uuid, "Testing project"))) }

        verify { synchronizationService.syncedNow(any()) }

        confirmVerified(synchronizationService)
        confirmVerified(applicationEventPublisher)
        confirmVerified(raynetProjectRepository)

    }

    "Webhook update test" {
        service.updateProject(Event(UUID.randomUUID().toString(), ZonedDateTime.now(), "record.updated", "user", Source("user", "edit"), EventData("Project", 1, arrayOf("2"))))

        verify { raynetProjectRepository.getByRaynetId(any()) }
        verify { raynetProjectRepository.save(any()) }

        verify { applicationEventPublisher.publishEvent(eq(CreateProjectEvent(uuid, "Testing project"))) }

        confirmVerified(applicationEventPublisher)
        confirmVerified(raynetProjectRepository)
    }

})
