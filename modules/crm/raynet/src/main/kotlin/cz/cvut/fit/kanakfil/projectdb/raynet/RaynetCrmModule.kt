package cz.cvut.fit.kanakfil.projectdb.raynet

import cz.cvut.fit.kanakfil.projectdb.commons.service.JobScheduler
import cz.cvut.fit.kanakfil.projectdb.commons.service.SchedulerParams
import cz.cvut.fit.kanakfil.projectdb.raynet.job.RaynetSyncJob
import cz.cvut.fit.kanakfil.projectdb.util.logger
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component

@Component
@ConditionalOnProperty(name = ["modules.crm.raynet"], havingValue = "enabled")
class RaynetCrmModule(private val jobScheduler: JobScheduler) {

    @EventListener(ApplicationReadyEvent::class)
    fun loadModule() {
        logger().info("Loaded module: raynet-crm")
        jobScheduler.scheduleJob(
            RaynetSyncJob.JOB_NAME,
            SchedulerParams(11, 26),
            RaynetSyncJob::class.java
        )
    }
}
