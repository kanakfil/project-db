package cz.cvut.fit.kanakfil.projectdb.raynet.service

import cz.cvut.fit.kanakfil.projectdb.commons.event.CreateProjectEvent
import cz.cvut.fit.kanakfil.projectdb.commons.event.ProjectCreatedEvent
import cz.cvut.fit.kanakfil.projectdb.commons.service.SynchronizationService
import cz.cvut.fit.kanakfil.projectdb.raynet.client.RaynetCrmClient
import cz.cvut.fit.kanakfil.projectdb.raynet.client.RaynetProperties
import cz.cvut.fit.kanakfil.projectdb.raynet.domain.RaynetProject
import cz.cvut.fit.kanakfil.projectdb.raynet.model.Event
import cz.cvut.fit.kanakfil.projectdb.raynet.model.ProjectDetailData
import cz.cvut.fit.kanakfil.projectdb.raynet.repository.RaynetProjectRepository
import cz.cvut.fit.kanakfil.projectdb.util.logger
import org.springframework.context.ApplicationEventPublisher
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Service
import java.time.format.DateTimeFormatter


@Service
class RaynetProjectService(
    private val raynetCrmClient: RaynetCrmClient,
    private val raynetProjectRepository: RaynetProjectRepository,
    private val synchronizationService: SynchronizationService,
    private val applicationEventPublisher: ApplicationEventPublisher,
    private val raynetProperties: RaynetProperties
) {

    fun updateProject(record: Event) {
        updateProject(record.data.entityId)
    }

    fun updateProject(project: ProjectDetailData) {
        val raynetProject =
            (raynetProjectRepository.getByRaynetId(project.id) ?: RaynetProject(project.id)).apply {
                name = project.name
                state = project.projectStatus.value
                stateId = project.projectStatus.id
            }
        saveProject(raynetProject)
    }

    fun updateProject(id: Long) {
        val project = raynetCrmClient.getProjectDetail(id)
        updateProject(project.data)
    }

    fun saveProject(project: RaynetProject) {
        val raynetProject = raynetProjectRepository.save(project)

        if (project.stateId == raynetProperties.createProjectStatus && raynetProject.projectId == null) {
            raynetProject.id
                ?.let { CreateProjectEvent(it, project.name ?: "") }
                ?.let {
                    applicationEventPublisher.publishEvent(it)
                    logger().debug("Event CreateProjectEvent sent: $", it)
                }
        }
    }

    @EventListener
    fun listenProjectCreated(event: ProjectCreatedEvent) {
        logger().debug("Received ProjectCreated event: $", event)
        event.id
            ?.let { raynetProjectRepository.findById(it) }
            ?.ifPresent {
                raynetProjectRepository.save(it.apply { projectId = event.projectId })
            }
    }

    fun updateAll() {
        val lastSync = synchronizationService.getLastSync(MODULE_NAME)
        raynetCrmClient.getNewAndModifiedProjects(
            lastSync.lastSync.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"))
        ).data.map(this::updateProject)
        synchronizationService.syncedNow(MODULE_NAME)
    }

    companion object {
        const val MODULE_NAME = "RAYNET"
    }

}
