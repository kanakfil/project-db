package cz.cvut.fit.kanakfil.projectdb.raynet.client

import feign.auth.BasicAuthRequestInterceptor
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration


@Configuration
class RaynetClientConfiguration(private val raynetProperties: RaynetProperties) {

    @Bean
    fun basicAuthRequestInterceptor(): BasicAuthRequestInterceptor {
        return BasicAuthRequestInterceptor(raynetProperties.user, raynetProperties.apiKey)
    }
}

@ConstructorBinding
@ConfigurationProperties(prefix = "modules.config.raynet-crm")
data class RaynetProperties(val user: String, val apiKey: String, val createProjectStatus: Long)
