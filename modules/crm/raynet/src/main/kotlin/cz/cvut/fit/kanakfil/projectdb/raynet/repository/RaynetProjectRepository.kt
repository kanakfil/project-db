package cz.cvut.fit.kanakfil.projectdb.raynet.repository

import cz.cvut.fit.kanakfil.projectdb.raynet.domain.RaynetProject
import org.springframework.data.repository.CrudRepository
import java.util.*

interface RaynetProjectRepository : CrudRepository<RaynetProject, UUID> {
    fun getByRaynetId(id: Long): RaynetProject?
}
