package cz.cvut.fit.kanakfil.projectdb.raynet.webhook

import cz.cvut.fit.kanakfil.projectdb.raynet.model.Event
import cz.cvut.fit.kanakfil.projectdb.raynet.service.RaynetProjectService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/public/webhook/raynet")
class WebhookController(private val raynetProjectService: RaynetProjectService) {

    @PostMapping
    fun getPublicInfo(@RequestBody record: Event): ResponseEntity<Void> {
        if (record.type == "record.updated" && record.data.entityName == "Project") {
            raynetProjectService.updateProject(record)
        }
        return ResponseEntity.noContent().build()
    }

}
