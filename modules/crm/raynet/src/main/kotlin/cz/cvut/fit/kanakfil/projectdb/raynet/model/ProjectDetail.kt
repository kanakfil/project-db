package cz.cvut.fit.kanakfil.projectdb.raynet.model

data class ProjectDetail(val data: ProjectDetailData)
data class ProjectDetailData(val id: Long, val projectStatus: ProjectStatus, val name: String)
data class ProjectStatus(val id: Long, val value: String)

data class ProjectList(val data: List<ProjectDetailData>)
