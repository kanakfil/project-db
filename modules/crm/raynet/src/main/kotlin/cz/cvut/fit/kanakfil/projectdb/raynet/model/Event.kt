package cz.cvut.fit.kanakfil.projectdb.raynet.model

import java.time.ZonedDateTime

data class Event(
    val eventId: String,
    val createdAt: ZonedDateTime,
    val type: String,
    val author: String,
    val source: Source,
    val data: EventData
)

data class Source(val name: String, val description: String)
data class EventData(val entityName: String, val entityId: Long, val extIds: Array<String>)
