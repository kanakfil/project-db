package cz.cvut.fit.kanakfil.projectdb.raynet.client

import cz.cvut.fit.kanakfil.projectdb.raynet.model.ProjectDetail
import cz.cvut.fit.kanakfil.projectdb.raynet.model.ProjectList
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestParam

@FeignClient(value = "raynet-crm-client", url = "https://app.raynet.cz/api/v2")
interface RaynetCrmClient {

    @GetMapping(value = ["/project/{projectId}"])
    fun getProjectDetail(@PathVariable("projectId") projectId: Long): ProjectDetail

    @GetMapping(value = ["/project/"])
    fun getNewAndModifiedProjects(@RequestParam("rowInfo.lastModifiedAt[GT]") createdAt: String): ProjectList

}
