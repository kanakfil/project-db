package cz.cvut.fit.kanakfil.projectdb.raynet.job

import cz.cvut.fit.kanakfil.projectdb.commons.service.JobData
import cz.cvut.fit.kanakfil.projectdb.commons.service.JobScheduler
import cz.cvut.fit.kanakfil.projectdb.raynet.service.RaynetProjectService
import cz.cvut.fit.kanakfil.projectdb.util.logger
import org.quartz.Job
import org.quartz.JobExecutionContext
import org.quartz.JobExecutionException
import org.springframework.stereotype.Component
import java.lang.String.format

@Component
class RaynetSyncJob(
    private val raynetProjectService: RaynetProjectService
) : Job {

    @Throws(JobExecutionException::class)
    override fun execute(jobExecutionContext: JobExecutionContext) {
        val jobDataMap = jobExecutionContext.mergedJobDataMap
        val refireCount = jobExecutionContext.refireCount
        val jobData = JobData(jobDataMap.getString(JobScheduler.JOB_NAME_KEY))
        try {
            if (refireCount > MAX_RETRIES) {
                throw JobExecutionException(
                    format(
                        "Job execution retries exceeded for job name %s",
                        jobData.jobName
                    )
                )
            }
            logger().info("Job execution for name: {}", jobData.jobName)
            raynetProjectService.updateAll()
        } catch (e: Exception) {
            val jobExecutionException = JobExecutionException(e)

            // fire it again
            jobExecutionException.setRefireImmediately(refireCount <= MAX_RETRIES)
            throw jobExecutionException
        }
    }

    companion object {
        const val JOB_NAME = "raynet_sync_job"
        private const val MAX_RETRIES = 3
    }
}
