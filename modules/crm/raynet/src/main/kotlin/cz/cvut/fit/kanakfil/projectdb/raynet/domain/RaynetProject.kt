package cz.cvut.fit.kanakfil.projectdb.raynet.domain

import org.hibernate.annotations.GenericGenerator
import org.hibernate.annotations.Type
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
class RaynetProject(
    @Column(name = "raynet_id", nullable = false, unique = true)
    val raynetId: Long? = null,

    @Column(name = "project_id", unique = false)
    var projectId: Long? = null,

    @Column(name = "state", nullable = false)
    var state: String? = null,

    @Column(name = "state_id", nullable = false)
    var stateId: Long? = null,

    @Column(name = "name", nullable = false)
    var name: String? = null,
) {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "id", nullable = false)
    @Type(type = "uuid-char")
    var id: UUID? = null
}
