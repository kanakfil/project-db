package cz.cvut.fit.kanakfil.projectdb.identitymanagement.keycloak

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class KeycloakApplicationTests : StringSpec(
    {

        "Test configuration should work" {
            true shouldBe true
        }

    }
)
