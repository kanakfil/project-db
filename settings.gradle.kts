rootProject.name = "projectdb"
include(":modules:commons")
project(":modules:commons").name = "commons"
include(":modules:identitymanagement:keycloak")
project(":modules:identitymanagement:keycloak").name = "keycloak"
include(":modules:projectmanagement:jira-connector")
project(":modules:projectmanagement:jira-connector").name = "jira-connector"
include(":modules:crm:raynet")
project(":modules:crm:raynet").name = "raynet"
include(":modules:vcs:gitlab")
project(":modules:vcs:gitlab").name = "gitlab"

